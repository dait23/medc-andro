import Home from './App/Home.react';
import Main from './App/Main';
//import Main from './App/Main';
// components
import Login from './Login';
import Splash from './Login/splash';
import Register from './Register';

import Reminder from './Reminder';
import NewReminder from './Reminder/newreminder';

import Aktivitas from './Aktivitas';
import Stopwatch from './Aktivitas/Stopwatch';
import BodyIndex from './Aktivitas/Body';
import StepCounter from './Aktivitas/Step';
import HeartRate from './Aktivitas/heart';

import Wanita from './Wanita';
import MasaSubur from './Wanita/masasubur';
import Kelahiran from './Wanita/kelahiran';

import Kids from './Kids';
import InputAnak from './Kids/input';
import ProfileKids from './Kids/profile.js';
import EditKids from './Kids/edit';
import KmsKids from './Kids/kms';
import InputKms from './Kids/timbangan';
import InputPosyandu from './Kids/inputPosyandu';
import Vitamin from './Kids/vitamin';
import Imunisasi from './Kids/imunisasi';
import Kembang from './Kids/kembanganak';

import Setting from './Setting';
import About from './Setting/about';
import Pass from './Setting/password';
import Profile from './Setting/profile';
import EditProfile from './Setting/editprofile';
import AddProfilePhoto from './Setting/addphoto';

import Medis from './Medis';
import InputMedis from './Medis/input';
import DetailMedis from './Medis/detail';



export default {
    home: {
        title: 'Dashboard',
        Page: Main,
    },
    splash: {
        title: 'splash',
        Page: Splash,
    },
    main: {
        title: 'Dashboard',
        Page: Main,
    },
    utama: {
        title: 'Dashboard',
        Page: Main,
    },
    login: {
        title: 'login',
        Page: Login,
    },
    signin: {
        title: 'login',
        Page: Login,
    },
    register: {
        title: 'Register',
        Page: Register,
    },
    reminder: {
        title: 'My Reminder',
        Page: Reminder,
    },
    newreminder: {
        title: 'New Reminder',
        Page: NewReminder ,
    },
    aktivitas: {
        title: 'My Activity',
        Page: Aktivitas,
    },
    stopwatch: {
        title: 'Stopwatch',
        Page: Stopwatch,
    },
    bodyindex: {
        title: 'Body Mass Index',
        Page: BodyIndex,
    },
    heart: {
        title: 'Heart Monitor',
        Page: HeartRate,
    },
    stepcounter: {
        title: 'My Step',
        Page: StepCounter,
    },
    wanita: {
        title: 'Kebidanan',
        Page: Wanita,
    },
    masasubur: {
        title: 'Hitung Masa Subur',
        Page: MasaSubur,
    },
    kelahiran: {
        title: 'Hitung Waktu Kelahiran',
        Page: Kelahiran,
    },
    kids: {
        title: 'Bayi & Anak',
        Page: Kids,
    },
    kms: {
        title: 'KMS Anak',
        Page: KmsKids,
    },
    inputanak: {
        title: 'Add Kids',
        Page: InputAnak,
    },
    timbangan: {
        title: 'Input Kms / Timbangan',
        Page: InputKms,
    },
    inputposyandu: {
        title: 'Add Posyandu',
        Page: InputPosyandu,
    },
    vitamin: {
        title: 'Jadwal Vitamin',
        Page: Vitamin,
    },
    imunisasi: {
        title: 'Jadwal Imunisasi',
        Page: Imunisasi,
    },
    kembang: {
        title: 'Perkembangan Balita',
        Page: Kembang,
    },
    profilekids: {
        title: 'Profile Anak',
        Page: ProfileKids,
    },
    editkids: {
        title: 'Edit Anak',
        Page: EditKids,
    },
    setting: {
        title: 'Settings',
        Page: Setting,
    },
    about: {
        title: 'About Us',
        Page: About,
    },
    pass: {
        title: 'Ubah Password',
        Page: Pass,
    },
    profile: {
        title: 'My Profile',
        Page: Profile,
    },
    editprofile: {
        title: 'Edit Profile',
        Page: EditProfile,
    },
    addphoto: {
        title: 'Add Photo',
        Page: AddProfilePhoto,
    },
    medis: {
        title: 'Medis & Labs',
        Page: Medis,
    },
    inputmedis: {
        title: 'Add Medis',
        Page: InputMedis,
    },
    detailmedis: {
        title: 'Detail Medis',
        Page: DetailMedis,
    },

};
