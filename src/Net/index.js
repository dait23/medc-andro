import React, { Component, PropTypes } from 'react';
import {Alert,  TouchableOpacity,  NetInfo, Dimensions, ToastAndroid, ScrollView, Platform, Text, View, StyleSheet, Image } from 'react-native';
import Iconx from 'react-native-vector-icons/MaterialIcons';

const deviceHeight = Dimensions.get('window').height;

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    onPress: PropTypes.func,
};
export default class NetPage extends Component {

    render(){

        return(

          <View style={{flex:1, backgroundColor:'#fff'}}>
          <View style={{alignItems:'center', justifyContent:'center', marginTop: deviceHeight / 4,}}>
          <Iconx name="signal-wifi-off"  size={100} color="#eee"/>
          <Text style={{color:'#888'}}>
            Internet Anda Terputus !!!
          </Text>
          </View>
          </View>
        );

    }

}

NetPage.propTypes = propTypes;
