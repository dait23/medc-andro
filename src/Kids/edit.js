import { Text, View, TextInput, DatePickerAndroid, Picker, StyleSheet, TouchableOpacity, ScrollView, ToastAndroid, Platform } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconx from 'react-native-vector-icons/FontAwesome';
import { Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  formContainer:{
    padding:15,
  },
  input:{
    height:40,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10,
  },
  inputPick:{
    height:40,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10,
    width:225
  },
  area:{
    height:70,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10
  },
  buttonContainer:{
    backgroundColor:COLOR.medcColor,
    paddingVertical:15,
    marginBottom:20,

  },
  buttonTitle:{
    textAlign:'center',
    color:'#fff',
    fontWeight:'700',

  },
  box:{
    flex:1,
    flexDirection:'row',
    justifyContent: 'space-around',
  },
  pick:{
    height:40,
    marginBottom:10,
    color:'#fff',
    textAlign:'center',
    width:100,
    justifyContent: 'center',
    paddingVertical:10,
  },
  camera:{
      alignItems:'center',
      marginBottom:30,
      marginTop:20
  }
});
const Item = Picker.Item;
const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class EditKids extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedItem: undefined,
      selected1: 'laki-laki',
      simpleText: 'Tanggal Lahir',
      results: {
        items: [],
      },
    };
  }

  onValueChange(value: string) {
    this.setState({
      selected1: value,
    });
  }
  renderToolbar = () => {
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
          />
      );
  }
  showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'Text'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'Text'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="Edit"/>

                    <View style={styles.formContainer}>
                    <View style={styles.camera}>
                     <Avatar icon="child-care" size={100} color={COLOR.medcColor}/>
                    </View>
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Nama Anak"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(password) => this.setState({password})}
                          returnKeyType="next"
                          value={this.state.password}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Tempat Lahir"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(password) => this.setState({password})}
                          returnKeyType="next"
                          value={this.state.password}
                        />
                        <View style={styles.box}>
                        <TextInput
                          style={styles.inputPick}
                          underlineColorAndroid='transparent'
                          placeholder="Day"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(password) => this.setState({password})}
                          returnKeyType="next"
                          value={this.state.simpleText}
                        />
                          <TouchableOpacity
                            onPress={this.showPicker.bind(this, 'simple', {date: this.state.simpleDate})}>
                            <View style={styles.pick}>
                              <Iconx name="calendar" size={28} color={COLOR.medcColor} style={{alignSelf:'center'}}/>
                            </View>
                          </TouchableOpacity>
                        </View>
                        <Picker
                          mode="dropdown"
                          selectedValue={this.state.selected1}
                          onValueChange={this.onValueChange.bind(this)}
                          style={styles.input}
                        >
                          <Item label="Laki-Laki" value="laki-laki" />
                          <Item label="Perempuan" value="perempuan" />
                        </Picker>
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Berat"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="numeric"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(password) => this.setState({password})}
                          returnKeyType="next"
                          value={this.state.password}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Tinggi"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="numeric"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(password) => this.setState({password})}
                          returnKeyType="next"
                          value={this.state.password}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Nama Ayah"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(password) => this.setState({password})}
                          returnKeyType="next"
                          value={this.state.password}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Nama Ibu"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(password) => this.setState({password})}
                          returnKeyType="next"
                          value={this.state.password}
                        />
                        <TextInput
                          style={styles.area}
                          underlineColorAndroid='transparent'
                          placeholder="Alamat"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(password) => this.setState({password})}
                          returnKeyType="next"
                          value={this.state.password}
                        />

                        <View style={styles.camera}>
                         <Avatar icon="camera-alt" size={80} color={COLOR.teal700}/>
                        </View>
                        <TouchableOpacity activeOpacity={.5} style={styles.buttonContainer} onPress={this.onSignUpPress}>
                          <Text style={styles.buttonTitle}>
                            UPDATE
                          </Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>


            </View>
        );
    }
}

EditKids.propTypes = propTypes;

export default EditKids;
