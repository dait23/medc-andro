import { Text, Dimensions, NetInfo, View, RefreshControl,StyleSheet, ScrollView, ToastAndroid, Platform, Image, TouchableOpacity, ActivityIndicator, Alert } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import NetPage from '../Net';
import ProfileKids from '../Kids/profile.js';
import {DevApi, MainApi} from '../Api';
import { IconToggle, Card, Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImageSlider from 'react-native-image-slider';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import Iconx from 'react-native-vector-icons/FontAwesome';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const welcomeImg = require('./../img/welcome.jpg');

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
    kidsbox:{
      alignSelf:'center',
      justifyContent: "center",
      height: deviceHeight / 3.8,
      marginTop:30,
      marginBottom:20,
    },
    kidsname:{
      fontSize:16,
      fontWeight:'bold',
      alignSelf:'center',
      marginTop:5,
    },
    kidsfemale:{
      color:COLOR.pink400,
      alignSelf:'center',
    },
    kidsmale:{
      color:COLOR.medcColor,
      alignSelf:'center',
    },
    actionButtonIcon: {
    fontSize: 30,
    height: 32,
    color: 'white',
  },
  centering: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
  },
  boxPicture:{
    flex:1,
    flexDirection:'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  avatarkid:{
    width:100,height:100,borderRadius:100,borderWidth:2,borderColor:COLOR.pink500, alignSelf:'center',resizeMode: 'cover',
  }
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};
class Kids extends Component {
  constructor(props) {
      super(props);
      this.getKids = this.getKids.bind(this)
      this.state = {
          selected: [],
          searchText: '',
          isConnected: true,
          data: [],
          loading: true,
          animating: true,
          refreshing: false,

      };
  }
  componentDidMount() {

      var that = this;
      this.getKids();
      that.setToggleTimeout();
      NetInfo.isConnected.addEventListener(
     'change',
     this._handleConnectivityChange
     );
     NetInfo.isConnected.fetch().done(
         (isConnected) => { this.setState({isConnected}); }
     );

  }
  componentWillUnmount() {
      clearTimeout(this._timer);
    NetInfo.isConnected.removeEventListener(
      'change',
      this._handleConnectivityChange
  );
}
_handleConnectivityChange = (isConnected) => {
this.setState({
  isConnected,
});
};

_onRefresh() {
  this.setState({refreshing: true});
  this.getKids().then(() => {
    this.setState({refreshing: false});
  });
}

getKidsDetail(id, title) {
    //dismissKeyboard();
    //Alert.alert(
    //  nama_Anak
    //)
  this.props.navigator.push({
    id: id,
    title: title,
    Page: ProfileKids,
  });
  }


setToggleTimeout() {
  this._timer = setTimeout(() => {
    this.setState({animating: !this.state.animating});
    this.setToggleTimeout();
  }, 2000);
}
getKids() {
    // Set loading to true to display a Spinner
    this.setState({
        loading: true
    });

    var url =  DevApi + 'kms/nama_anak';
    var that = this;
    return fetch(url, {
            method: 'GET',
            headers: {
              'Client-Service': 'mobile-client',
              'Content-Type': 'application/json',
              'Auth-key': 'medcrestapi'
            }
      })
      .then(function(response) {
        return response.json();
      }).then(function(result) {
        //componentWillUnmount();
        that.setState({ data : result,
                        loading: false

                      });
         //data : this.state.data(result.data),

      })
      .catch((error) => { console.error(error); });
}
renderLoading(){

  if (this.state.loading) {
    return (
      <View>
      <ActivityIndicator
      animating={this.state.animating}
      style={[styles.centering, {height: 50}]}
      size="small"
      color={COLOR.pink500}
     />
      </View>

      );

  }else{

      if(this.state.data.status == '200'){

        return(
           <View style={styles.boxPicture}>
           {this.state.data.data.map((item, i)=>{

             if(item.kelamin=='perempuan'){

                return(

                  <View style={styles.kidsbox}>
                  <TouchableOpacity activeOpacity={.9}  onPress={() => this.getKidsDetail(item.id_anak, item.nama_anak)}>
                     <Image source={{ uri: item.link_picture }} style={styles.avatarkid}/>
                  </TouchableOpacity>
                    <Text style={styles.kidsname}>{item.nama_anak}</Text>
                    <Text style={styles.kidsfemale}>{item.kelamin}</Text>
                  </View>

                );

             }else{

               return(

                 <View style={styles.kidsbox}>
                    <Image source={{ uri: item.link_picture }} style={styles.avatarkid}/>
                   <Text style={styles.kidsname}>{item.nama_anak}</Text>
                   <Text style={styles.kidsmale}>{item.kelamin}</Text>
                 </View>

               );
             }

            })}
           </View>
        );
      }else{

        return (

          <View>
            <Text style={{ alignSelf: 'center' , marginTop:50}}>Belum Ada anak </Text>
          </View>


        );
      }

  }


}

RenderOffline(){

  if(this.state.isConnected == false){

      return(
          <View>
           <NetPage />
          </View>

      );
  }else{

    return(

      <View>
      {this.renderMain()}
      </View>
    );
  }

}

renderMain= () =>{

   return(

        <View>
        {this.renderLoading()}

            <Subheader text="List"/>
              {this.renderItem('KMS', COLOR.medcColor, 'child-care')}
              {this.renderItem('KMS', COLOR.pink400, 'child-care')}
        </View>

   );
}
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              searchable={{
                  autoFocus: true,
                  placeholder: 'Search',
                  onChangeText: value => this.setState({ searchText: value }),
                  onSearchClosed: () => this.setState({ searchText: '' }),
              }}
          />
      );
  }
  renderItem = (title, color, icon) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={40} color={color}/>}
            centerElement={title}
            rightElement="chevron-right"
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView
                style={styles.container}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh.bind(this)}
                  />
                }
                >

                    <View style={{marginTop:0, backgroundColor:'#f9f9f9'}}>

                    {this.RenderOffline()}
                    </View>

                </ScrollView>

                <ActionButton buttonColor={COLOR.pink500} bgColor="rgba(0,0,0,0.8)">
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Input Anak"  onPress={() => this.props.navigator.push(routes.inputanak)}>
                      <Icon name="person-add" style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="KMS"  onPress={() => this.props.navigator.push(routes.kms)}>
                      <Icon name="child-care" style={styles.actionButtonIcon} />
                    </ActionButton.Item>

                </ActionButton>

            </View>
        );
    }
}

Kids.propTypes = propTypes;

export default Kids;
