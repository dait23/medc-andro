import { Text, Dimensions, View, StyleSheet, ScrollView, ToastAndroid, Platform, Image } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {IconToggle, Card, Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImageSlider from 'react-native-image-slider';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const vitaminA = require('./../img/vitamin-pria.jpg');
const imun = require('./../img/imunisasi-pria.jpg');

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
    banner:{
        height: deviceHeight / 4.5,
        backgroundColor:COLOR.grey200,

    },
    actionButtonIcon: {
      fontSize: 30,
      height: 32,
    color: 'white',
  },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};
class Imunisasi extends Component {
  constructor(props) {
      super(props);

      this.state = {
          selected: [],
          searchText: '',

      };
  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}

          />
      );
  }
  renderItem = (title, color, icon) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={40} color={color}/>}
            centerElement={title}
            rightElement="delete"
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <View style={{marginTop:0, backgroundColor:'#fff'}}>

                      <Image source={imun}  style={{  width: deviceWidth / 1, alignSelf:'center', resizeMode:'contain'}}/>

                    </View>

                </ScrollView>

            </View>
        );
    }
}

Imunisasi.propTypes = propTypes;

export default Imunisasi;
