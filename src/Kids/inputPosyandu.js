import {
  Text,
  View,
  TextInput,
  DatePickerAndroid,
  Picker,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ToastAndroid,
  Platform,
  Image,
  ActivityIndicator
 } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconx from 'react-native-vector-icons/FontAwesome';
import { Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  formContainer:{
    padding:15,
  },
  input:{
    height:40,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10,
  },
  inputPick:{
    height:40,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10,
    width:225
  },
  area:{
    height:70,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10
  },
  buttonContainer:{
    backgroundColor:COLOR.medcColor,
    paddingVertical:15,
    marginBottom:20,

  },
  buttonTitle:{
    textAlign:'center',
    color:'#fff',
    fontWeight:'700',

  },
  box:{
    flex:1,
    flexDirection:'row',
    justifyContent: 'space-around',
  },
  pick:{
    height:40,
    marginBottom:10,
    color:'#fff',
    textAlign:'center',
    width:100,
    justifyContent: 'center',
    paddingVertical:10,
  },
  camera:{
      alignItems:'center',
      marginBottom:30,
      marginTop:20
  },
  image: {
    height: 100,
    width:100,
    borderRadius:100,
    alignSelf:'center',

  },
});
const Item = Picker.Item;
const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class InputPosyandu extends Component {

  constructor(props) {
    super(props);
    this.onPosyandu = this.onPosyandu.bind(this);
    this.state = {
      selectedItem: undefined,
      simpleText: 'Tanggal',
      data:{},
      results: {
        items: [],
      },
    };
  }


  focusChangeField = (focusField) => {
    this.refs[focusField].focus();
  }

  renderToolbar = () => {
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
          />
      );
  }
  showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'Text'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'Text'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };
  onPosyandu= () =>  {

      var url =  DevApi + 'kms/add_posyandu';
      var that = this;
      return fetch(url, {
              method: 'POST',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              },
              body: JSON.stringify({

                'nama' : this.state.nama,
                'alamat' : this.state.alamat,
                'keterangan' : this.state.keterangan,
                'petugas': this.state.petugas



              }),
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          if (result.data.status == '200') {
            ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
            that.props.navigator.pop();
            that.setState({ data : result.data });
          }
          else{
          ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
        }
        })
        .catch((error) => { console.error(error); });
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="Add New"/>
                    <View style={styles.formContainer}>
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Nama Posyandu"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(nama) => this.setState({nama})}
                          ref="namaPosyandu"
                          onSubmitEditing={() => this.focusChangeField('alamatPosyandu')}
                          returnKeyType="next"
                          value={this.state.nama}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Alamat Posyandu"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(alamat) => this.setState({alamat})}
                          ref="alamatPosyandu"
                          onSubmitEditing={() => this.focusChangeField('keteranganPosyandu')}
                          returnKeyType="next"
                          value={this.state.alamat}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="keterangan"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(keterangan) => this.setState({keterangan})}
                          ref="keteranganPosyandu"
                          onSubmitEditing={() => this.focusChangeField('petugasPosyandu')}
                          returnKeyType="next"
                          value={this.state.keterangan}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Petugas Posyandu"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          ref="petugasPosyandu"
                          onChangeText={(petugas) => this.setState({petugas})}
                          returnKeyType="done"
                          value={this.state.petugas}
                        />
                        <TouchableOpacity activeOpacity={.5} style={styles.buttonContainer} onPress={this.onPosyandu}>
                          <Text style={styles.buttonTitle}>
                            ADD POSYANDU
                          </Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>


            </View>
        );
    }
}

InputPosyandu.propTypes = propTypes;

export default InputPosyandu;
