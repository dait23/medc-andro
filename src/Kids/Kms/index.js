import { Text, Dimensions,  NetInfo, View, StyleSheet, ScrollView, ToastAndroid, Platform, Image } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {IconToggle, Card, Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImageSlider from 'react-native-image-slider';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconz from 'react-native-vector-icons/FontAwesome';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const welcomeImg = require('./../img/welcome.jpg');

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
    banner:{
        height: deviceHeight / 4.5,
        backgroundColor:COLOR.grey200,

    },
    actionButtonIcon: {
      fontSize: 30,
      height: 32,
    color: 'white',
  },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};
class KmsKids extends Component {
  constructor(props) {
      super(props);

      this.state = {
          selected: [],
          searchText: '',
          isConnected: true,

      };
  }
  componentDidMount() {

      //var that = this;
      ///this.profile();
      NetInfo.isConnected.addEventListener(
     'change',
     this._handleConnectivityChange
     );
     NetInfo.isConnected.fetch().done(
         (isConnected) => { this.setState({isConnected}); }
     );

  }
  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'change',
      this._handleConnectivityChange
  );
  }
  _handleConnectivityChange = (isConnected) => {
  this.setState({
  isConnected,
  });
  };
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              searchable={{
                  autoFocus: true,
                  placeholder: 'Search',
                  onChangeText: value => this.setState({ searchText: value }),
                  onSearchClosed: () => this.setState({ searchText: '' }),
              }}
          />
      );
  }
  renderItem = (title, color, icon) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={40} color={color}/>}
            centerElement={title}
            rightElement="delete"
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <View style={{marginTop:0, backgroundColor:'#f9f9f9'}}>

                    <Row size={12} style={styles.Rm}>
                     <Col sm={12} md={12} lg={12}>
                       <View style={styles.banner}>
                       </View>
                     </Col>
                    </Row>

                    <Subheader text="Riwayat Medis"/>

                    {this.renderItem('Riwayat Medis 1', COLOR.tealA700, 'local-hospital')}
                    {this.renderItem('Riwayat Medis 2', COLOR.tealA700, 'local-hospital')}
                    {this.renderItem('Riwayat Medis 3', COLOR.tealA700, 'local-hospital')}
                    {this.renderItem('Riwayat Medis 4', COLOR.tealA700, 'local-hospital')}
                    {this.renderItem('Riwayat Medis 5', COLOR.tealA700, 'local-hospital')}

                  <Subheader text="Medical Labs"/>
                    {this.renderItem('Cek Lab 1', COLOR.teal700, 'local-pharmacy')}
                    {this.renderItem('Cek Lab 2', COLOR.teal700, 'local-pharmacy')}
                    {this.renderItem('Cek Lab 3', COLOR.teal700, 'local-pharmacy')}
                    {this.renderItem('Cek Lab 4', COLOR.teal700, 'local-pharmacy')}
                    {this.renderItem('Cek Lab 5', COLOR.teal700, 'local-pharmacy')}



                    </View>

                </ScrollView>

                <ActionButton buttonColor={COLOR.pink500} bgColor="rgba(0,0,0,0.8)">
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Input Medis"  onPress={() => this.props.navigator.push(routes.inputmedis)}>
                      <Icon name="local-hospital" style={styles.actionButtonIcon} />
                    </ActionButton.Item>

                </ActionButton>

            </View>
        );
    }
}

KmsKids.propTypes = propTypes;

export default KmsKids;
