import {
  Text,
  View,
  TextInput,
  DatePickerAndroid,
  Picker,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ToastAndroid,
  Platform,
  Image,
  ActivityIndicator
 } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconx from 'react-native-vector-icons/FontAwesome';
import { Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  formContainer:{
    padding:15,
  },
  input:{
    height:40,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10,
  },
  inputPick:{
    height:40,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10,
    width:225
  },
  area:{
    height:70,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10
  },
  buttonContainer:{
    backgroundColor:COLOR.medcColor,
    paddingVertical:15,
    marginBottom:20,

  },
  buttonTitle:{
    textAlign:'center',
    color:'#fff',
    fontWeight:'700',

  },
  box:{
    flex:1,
    flexDirection:'row',
    justifyContent: 'space-around',
  },
  pick:{
    height:40,
    marginBottom:10,
    color:'#fff',
    textAlign:'center',
    width:100,
    justifyContent: 'center',
    paddingVertical:10,
  },
  camera:{
      alignItems:'center',
      marginBottom:30,
      marginTop:20
  },
  image: {
    height: 100,
    width:100,
    borderRadius:100,
    alignSelf:'center',

  },
});
const Item = Picker.Item;
const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class InputKms extends Component {

  constructor(props) {
    super(props);
    this.onAddKids = this.onAddKids.bind(this);
    this.state = {
      selectedItem: undefined,
      selected1: 'Raya',
      simpleText: 'Tanggal',
      datax:[],
      results: {
        items: [],
      },
    };
  }

  componentDidMount() {

      var that = this;
      this.getKids();
  }
  focusChangeField = (focusField) => {
    this.refs[focusField].focus();
  }

  onValueChange(value: string) {
    this.setState({
      selected1: value,
    });
  }
  renderToolbar = () => {
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
          />
      );
  }
  showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'Text'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'Text'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };
  getKids= () => {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });

      var url =  DevApi + 'kms/nama_anak';
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          //componentWillUnmount();
          that.setState({ datax : result,
                          loading: false

                        });
           //data : this.state.data(result.data),

        })
        .catch((error) => { console.error(error); });
  }
  onAddKids= () =>  {

      var url =  DevApi + 'timbangan/add_timbangan';
      var that = this;
      return fetch(url, {
              method: 'POST',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              },
              body: JSON.stringify({
                'id_anak' : this.state.selected1,
                'umur_anak' : this.state.umurAnak,
                'tanggal' : this.state.simpleText,
                'berat': this.state.berat,
                'kbm': this.state.kbm,
                'asi': this.state.asi,
                'keterangan': this.state.keterangan


              }),
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          if (result.data.status == '200') {
            ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
            that.props.navigator.pop();
            that.setState({ data : result.data });
          }
          else{
          ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
        }
        })
        .catch((error) => { console.error(error); });
    }

    renderMedis(){



          if(this.state.datax.status == '200'){



                  return (

                   <View>
                   <Picker
                     mode="dropdown"
                     selectedValue={this.state.selected1}
                     onValueChange={this.onValueChange.bind(this)}
                     style={styles.input}
                   >
                   {this.state.datax.data.map((item, i) => {
                       return <Picker.Item
                                key={i}
                                value={item.id_anak}
                                label={item.nama_anak} />
                    }) }
                      </Picker>
                   </View>

                  );

          }else{

            return (

              <View>
                <Text style={{ alignSelf: 'center' }}>No Medis Record</Text>
              </View>


            );
          }

      }


    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="Input Timbangan"/>


                    <View style={styles.formContainer}>




                      {this.renderMedis()}
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Umur Anak Anda"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(umurAnak) => this.setState({umurAnak})}
                          ref="namaAnak"
                          returnKeyType="next"
                          value={this.state.umurAnak}
                        />
                        <View style={styles.box}>
                        <TextInput
                          style={styles.inputPick}
                          underlineColorAndroid='transparent'
                          placeholder="Day"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(simpleText) => this.setState({simpleText})}
                          ref="simpleText"
                          onSubmitEditing={() => this.focusChangeField('simpleText')}
                          returnKeyType="next"
                          value={this.state.simpleText}
                        />
                          <TouchableOpacity
                            onPress={this.showPicker.bind(this, 'simple', {date: this.state.simpleDate})}>
                            <View style={styles.pick}>
                              <Iconx name="calendar" size={28} color={COLOR.medcColor} style={{alignSelf:'center'}}/>
                            </View>
                          </TouchableOpacity>
                        </View>

                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Berat"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="numeric"
                          autoCapitalize="none"
                          autoCorrect={false}
                          ref="berat"
                          onChangeText={(berat) => this.setState({berat})}
                          onSubmitEditing={() => this.focusChangeField('tinggi')}
                          returnKeyType="next"
                          value={this.state.berat}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Kbm"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          ref="kbm"
                          onChangeText={(kbm) => this.setState({kbm})}
                          onSubmitEditing={() => this.focusChangeField('asi')}
                          returnKeyType="next"
                          value={this.state.kbm}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Asi Ekslusif"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(asi) => this.setState({asi})}
                          onSubmitEditing={() => this.focusChangeField('keterangan')}
                          ref="asi"
                          returnKeyType="next"
                          value={this.state.asi}
                        />

                        <TextInput
                          style={styles.area}
                          underlineColorAndroid='transparent'
                          placeholder="Keterangan"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          ref="keterangan"
                          onChangeText={(keterangan) => this.setState({keterangan})}
                          returnKeyType="done"
                          value={this.state.keterangan}
                        />

                        <TouchableOpacity activeOpacity={.5} style={styles.buttonContainer} onPress={this.onAddKids}>
                          <Text style={styles.buttonTitle}>
                            INPUT
                          </Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>


            </View>
        );
    }
}

InputKms.propTypes = propTypes;
export default InputKms;
