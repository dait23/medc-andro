import { Text, Dimensions,  NetInfo, RefreshControl, View, StyleSheet, ScrollView, ToastAndroid, Platform, Image } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import NetPage from '../Net';
import { ActionButton, IconToggle, Card, Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImageSlider from 'react-native-image-slider';
import Iconx from 'react-native-vector-icons/FontAwesome';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
import Moment from 'moment';
Moment.locale('id') //For Turkey
const welcomeImg = require('./../img/welcome.jpg');

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
    kidsbox:{
      alignItems:'center',
      alignSelf:'center',
      justifyContent: "center",
      height: deviceHeight / 3.8,
      marginTop:30,
      marginBottom:20,
    },
    kidsname:{
      fontSize:16,
      fontWeight:'bold',
      alignSelf:'center',
      marginTop:5,
      marginBottom:5,
    },
    kidsfemale:{
      color:COLOR.pink400,
      alignSelf:'center',
    },
    kidsmale:{
      color:COLOR.medcColor,
      alignSelf:'center',
    },
    boxPicture:{
      flex:1,
      flexDirection:'row',
      justifyContent: 'space-around',
      alignItems: 'center'
    },

});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    title: PropTypes.object.isRequired,
};
class ProfileKids extends Component {
  constructor(props) {
      super(props);

      this.state = {
          selected: [],
          searchText: '',
          data : {},
          refreshing: false,
           isConnected: true,
      };
  }
  componentDidMount() {

      var that = this;
      this.profileKid();
      NetInfo.isConnected.addEventListener(
     'change',
     this._handleConnectivityChange
     );
     NetInfo.isConnected.fetch().done(
         (isConnected) => { this.setState({isConnected}); }
     );

  }
  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'change',
      this._handleConnectivityChange
  );
}
_handleConnectivityChange = (isConnected) => {
this.setState({
  isConnected,
});
};
  _onRefresh() {
    this.setState({refreshing: true});
    this.profileKid().then(() => {
      this.setState({refreshing: false});
    });
  }
  profileKid() {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });

      var ID = this.props.route.id
      var url =  DevApi + 'kms/anak/detail/' + ID;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              }
        })
        .then(function(response) {
        return response.json();
      }).then(function(result) {
        that.setState({
          data : result.data,
          nama_anak:result.data.nama_anak,
          tempat_lahir: result.data.tempat_lahir,
          tgl_lhr: result.data.tgl_lhr,
          kelamin:result.data.kelamin,
          alamat:result.data.alamat,
          berat:result.data.berat,
          tinggi:result.data.tinggi,
          nama_ayah:result.data.nama_ayah,
          nama_ibu:result.data.nama_ibu,
          link_picture: result.data.link_picture,
          loading: false
         });
      })
      .catch((error) => { console.error(error); });
  }
  RenderOffline(){

    if(this.state.isConnected == false){

        return(
            <View>
             <NetPage />
            </View>

        );
    }else{

       return(

         <View>
         {this.renderMain()}
         </View>
       )
    }
  }
  renderMain= () =>{

    const dateTime= this.state.tgl_lhr;

    const formattedDT = Moment(dateTime).format('LL') //2 May
    const lahir = this.state.tempat_lahir + "," + " " + " " + formattedDT


    return(

      <View>
      {this.renderpicture()}

      <Subheader text="Biodata"/>
        {this.renderItem(this.state.nama_anak, COLOR.medcColor, 'person')}
        {this.renderItem(lahir, COLOR.medcColor,'home')}
        {this.renderItem(this.state.nama_ayah, COLOR.medcColor,'person')}
        {this.renderItem(this.state.nama_ibu, COLOR.medcColor, 'person')}
        {this.renderItem(this.state.alamat, COLOR.medcColor,'home')}

      </View>

    );


  }
  renderpicture(){

    if(this.state.kelamin =='perempuan'){

      return(

        <View style={styles.kidsbox}>

           <Image source={{ uri: this.state.link_picture }} style={{width:150,height:150,borderRadius:150,borderWidth:2,borderColor:COLOR.pink500, alignSelf:'center',resizeMode: 'cover',}}/>

          <Text style={styles.kidsname}>{this.state.nama_anak}</Text>
          <Text style={styles.kidsfemale}>{this.state.kelamin}</Text>
        </View>


      );

    }else{

      return(
        <View style={styles.kidsbox}>
           <Image source={{ uri: this.state.link_picture }} style={{width:150,height:150,borderRadius:150,borderWidth:2,borderColor:COLOR.medcColor, alignSelf:'center'}}/>
           <Text style={styles.kidsname}>{this.state.nama_anak}</Text>
           <Text style={styles.kidsmale}>{this.state.kelamin}</Text>
        </View>
      );

    }
  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              
          />
      );
  }
  renderItem = (title, color, icon) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={40} color={color}/>}
            centerElement={title}

        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView
                style={styles.container}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh.bind(this)}
                  />
                }
                >
                  <View style={{marginTop:0, backgroundColor:'#f9f9f9'}}>
                   {this.RenderOffline()}
                  </View>
                </ScrollView>
            </View>
        );
    }
}

ProfileKids.propTypes = propTypes;

export default ProfileKids;
