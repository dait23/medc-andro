import { Text, TouchableOpacity,  RefreshControl , NetInfo, Dimensions, View, ActivityIndicator, StyleSheet, ScrollView, ToastAndroid, Platform, Image } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import NetPage from '../Net';
import {DevApi, MainApi} from '../Api';
import {IconToggle, Card, Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImageSlider from 'react-native-image-slider';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const welcomeImg = require('./../img/welcome2.jpg');

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
    banner:{
        height: deviceHeight / 4.5,
        backgroundColor:COLOR.grey200,

    },
    actionButtonIcon: {
      fontSize: 30,
      height: 32,
    color: 'white',
  },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};
class KmsKids extends Component {
  constructor(props) {
      super(props);
      this.getPosyandu = this.getPosyandu.bind(this)
      this.state = {
          selected: [],
          searchText: '',
          isConnected: true,
          data: [],
          loading: true,
          animating: true,
          refreshing: false,
          isConnected: true,

      };
  }
  componentDidMount() {

    var that = this;
    this.getPosyandu();
    //this.getRiwayat();
    that.setToggleTimeout();
      NetInfo.isConnected.addEventListener(
     'change',
     this._handleConnectivityChange
     );
     NetInfo.isConnected.fetch().done(
         (isConnected) => { this.setState({isConnected}); }
     );

  }
  componentWillUnmount() {
      clearTimeout(this._timer);
    NetInfo.isConnected.removeEventListener(
      'change',
      this._handleConnectivityChange
  );
  }
  _handleConnectivityChange = (isConnected) => {
  this.setState({
  isConnected,
  });
  };
  _onRefresh() {
    this.setState({refreshing: true});
    this.getPosyandu().then(() => {
      this.setState({refreshing: false});
    });
  }
  setToggleTimeout() {
    this._timer = setTimeout(() => {
      this.setState({animating: !this.state.animating});
      this.setToggleTimeout();
    }, 2000);
  }
getPosyandu() {
    // Set loading to true to display a Spinner
    this.setState({
        loading: true
    });

    var url =  DevApi + 'kms/posyandu';
    var that = this;
    return fetch(url, {
            method: 'GET',
            headers: {
              'Client-Service': 'mobile-client',
              'Content-Type': 'application/json',
              'Auth-key': 'medcrestapi'
            }
      })
      .then(function(response) {
        return response.json();
      }).then(function(result) {
        //componentWillUnmount();
        that.setState({ data : result,
                        loading: false

                      });
         //data : this.state.data(result.data),

      })
      .catch((error) => { console.error(error); });
}
renderPosyandu(){

  if (this.state.loading) {
    return (
      <View>
      <ActivityIndicator
      animating={this.state.animating}
      style={[styles.centering, {height: 50}]}
      size="small"
      color={COLOR.pink500}
     />
      </View>

      );

  }else{

      if(this.state.data.status == '200'){

        return(
           <View>
           {this.state.data.data.map((item, i)=>{

              return (

               <View>
                  {this.renderItem(item.nama, COLOR.tealA700, 'child-care')}
               </View>

              );

            })}
           </View>
        );
      }else{

        return (

          <View>
            <Text style={{ alignSelf: 'center' }}>No Posyandu Record</Text>
          </View>


        );
      }

  }


}
RenderOffline(){

  if(this.state.isConnected == false){

      return(
          <View>
           <NetPage />
          </View>

      );
  }else{

     return(

       <View>
       {this.renderMain()}
       </View>
     )
  }
}
  renderAktivits= () =>{

    return(

      <View>
      <Subheader text="Aktivitas"/>
      {this.renderItem('February, 21 2017', COLOR.tealA700, 'child-care')}
      {this.renderItem('February, 21 2017', COLOR.tealA700, 'child-care')}

      </View>

    );

  }
  renderMain= () =>{

    return(
      <View>
      <Subheader text="Posyandu List"/>
      {this.renderPosyandu()}
      </View>

    );
  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              searchable={{
                  autoFocus: true,
                  placeholder: 'Search',
                  onChangeText: value => this.setState({ searchText: value }),
                  onSearchClosed: () => this.setState({ searchText: '' }),
              }}
          />
      );
  }
  renderItem = (title, color, icon) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={40} color={color}/>}
            centerElement={title}
            rightElement="chevron-right"
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView
                style={styles.container}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh.bind(this)}
                  />
                }
                >
                <ImageSlider images={[
                     welcomeImg
                 ]}/>
                    <View style={{marginTop:0, backgroundColor:'#fff'}}>




                    {this.RenderOffline()}


                    </View>

                </ScrollView>

                <ActionButton buttonColor={COLOR.pink500} bgColor="rgba(0,0,0,0.8)">
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Input Timbangan Anak"  onPress={() => this.props.navigator.push(routes.timbangan)}>
                      <Icon name="local-hospital" style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Jadwal Vitamin"  onPress={() => this.props.navigator.push(routes.vitamin)}>
                      <Icon name="local-pharmacy" style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Jadwal Imunisasi"  onPress={() => this.props.navigator.push(routes.imunisasi)}>
                      <Icon name="local-pharmacy" style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Perkembangan Anak"  onPress={() => this.props.navigator.push(routes.kembang)}>
                      <Icon name="child-care" style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Add Posyandu"  onPress={() => this.props.navigator.push(routes.inputposyandu)}>
                      <Icon name="local-hospital" style={styles.actionButtonIcon} />
                    </ActionButton.Item>

                </ActionButton>

            </View>
        );
    }
}

KmsKids.propTypes = propTypes;

export default KmsKids;
