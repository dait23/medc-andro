import { Text, View, StyleSheet, ScrollView, ToastAndroid, Platform } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import { ActionButton, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import Icon from 'react-native-vector-icons/FontAwesome';
const myIcon = (<Icon name="rocket" size={30} color="#900" />)
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class About extends Component {
  constructor(props) {
      super(props);

      this.state = {
          selected: [],
          searchText: '',

      };
  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}

          />
      );
  }
  renderItem = (title, color, icon, route) => {
      
      return (

        <ListItem
            divider
            leftElement={<Icon name={icon} size={26} color={color} />}
            centerElement={title}
            rightElement="chevron-right"
            onPress={() => this.props.navigator.push(route)}
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="Medclabs Android App"/>
                    <Subheader text="Version 1.0.2"/>



                </ScrollView>
            </View>
        );
    }
}

About.propTypes = propTypes;

export default About;
