import { Text, View, StyleSheet, ScrollView, ToastAndroid, Platform, Alert, AsyncStorage } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import { ActionButton, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import Icon from 'react-native-vector-icons/FontAwesome';
const myIcon = (<Icon name="rocket" size={30} color="#900" />)
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class Setting extends Component {
  constructor(props) {
      super(props);
      this.onLogoutPress = this.onLogoutPress.bind(this);
      this.state = {
          selected: [],
          searchText: '',

      };
  }
  onLogoutPress() {

      //var url = 'http://apis.inginkemana.com/index.php/auth/logout';
      var url =  DevApi + 'auth/logout';
      var that = this;
      return fetch(url, {
              method: 'POST',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              },

        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
            if (result.data.status == '200') {
                //AsyncStorage.removeItem('@userDetails');
                //AsyncStorage.removeItem('@userDetails');
                //AsyncStorage.setItem("@userLoggedIn","false");
                that.props.navigator.push(routes.signin)
                ToastAndroid.show("SignOut Sukses", ToastAndroid.SHORT)

            }else{

            ToastAndroid.show(result.data.message, ToastAndroid.SHORT)

            }


          that.setState({ data : result.data });
        })
        .catch((error) => { console.error(error); });
    }


  _logoutHandle = async () => {
    try {
      await AsyncStorage.removeItem('@userDetails');
      await AsyncStorage.setItem("@userLoggedIn","false");
      this.props.navigator.push(routes.signin)
      ToastAndroid.show("SignOut Sukses", ToastAndroid.SHORT)
    } catch (error) {
      ToastAndroid.show("SignOut Gagal", ToastAndroid.SHORT)
    }
 }


  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              rightElement="lock-outline"
              onRightElementPress={this.onLogoutPress}

          />
      );
  }
  renderItem = (title, color, icon, route) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Icon name={icon} size={26} color={color} />}
            centerElement={title}
            rightElement="chevron-right"
            onPress={() => this.props.navigator.push(route)}

        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="My Setting"/>

                    <View style={{marginTop:10}}>
                    {this.renderItem('My profile', COLOR.grey600, 'user-circle', routes.profile)}
                    {this.renderItem('Change Password', COLOR.grey600,'unlock-alt', routes.pass)}
                    {this.renderItem('About Us', COLOR.grey600,'android', routes.about)}
                    </View>

                </ScrollView>
            </View>
        );
    }
}

Setting.propTypes = propTypes;

export default Setting;
