import {
  Text,
  TextInput,
  Picker,
  DatePickerAndroid,
  TouchableOpacity,
  View,StyleSheet,
  ScrollView,
  ToastAndroid,
  Platform,
  Image,
  ActivityIndicator } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import ImagePicker from 'react-native-image-picker'
import RNFetchBlob from 'react-native-fetch-blob'
import FbApp from '../Firebase/firebase'
import { Avatar, Card, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconM from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconx from 'react-native-vector-icons/FontAwesome';

const styles = StyleSheet.create({
  container: {
      flex: 1,
  },
  actionButtonIcon: {
  fontSize: 20,
  height: 22,
  color: 'white',
},
formContainer:{
  padding:15,
},
input:{
  height:40,
  backgroundColor:COLOR.grey100,
  marginBottom:10,
  color:'#000',
  paddingLeft:10,
},
inputPick:{
  height:40,
  backgroundColor:COLOR.grey100,
  marginBottom:10,
  color:'#000',
  paddingLeft:10,
  width:225
},
area:{
  height:70,
  backgroundColor:COLOR.grey100,
  marginBottom:10,
  color:'#000',
  paddingLeft:10
},
buttonContainer:{
  backgroundColor:COLOR.medcColor,
  paddingVertical:15,
  marginBottom:20,

},
buttonTitle:{
  textAlign:'center',
  color:'#fff',
  fontWeight:'700',

},
box:{
  flex:1,
  flexDirection:'row',
  justifyContent: 'space-around',
},
pick:{
  height:40,
  marginBottom:10,
  textAlign:'center',
  width:100,
  justifyContent: 'center',
  paddingVertical:10,
},
camera:{
    alignItems:'center',
    marginBottom:30,
    marginTop:20
},
image: {
  height: 100,
  width:100,
  borderRadius:100,
  alignSelf:'center',

},
upload: {
  textAlign: 'center',
  color: '#333333',
  padding: 10,
  marginBottom: 5,
  borderWidth: 1,
  borderColor: 'gray'
},
});



const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};
const storage = FbApp.storage();

// Prepare Blob support
const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

const uploadImage = (uri, mime = 'application/octet-stream') => {
  return new Promise((resolve, reject) => {
    const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
    const sessionId = new Date().getTime()
    let uploadBlob = null
    const imageRef = storage.ref('images').child(`${sessionId}`)

    fs.readFile(uploadUri, 'base64')
      .then((data) => {
        return Blob.build(data, { type: `${mime};BASE64` })
      })
      .then((blob) => {
        uploadBlob = blob
        return imageRef.put(blob, { contentType: mime })
      })
      .then(() => {
        uploadBlob.close()
        return imageRef.getDownloadURL()
      })
      .then((url) => {
        resolve(url)
      })
      .catch((error) => {
        reject(error)
    })
  })
}
class AddProfilePhoto extends Component {
  constructor(props) {
      super(props);
      this.onUploadPhoto = this.onUploadPhoto.bind(this);
      this.state = {
          selected: [],
          searchText: '',


      };
  }
  _pickImage() {
    this.setState({ uploadURL: '' })

    ImagePicker.showImagePicker({}, response  => {
      uploadImage(response.uri)
        .then(url => this.setState({ uploadURL: url }))
        .catch(error => console.log(error))
    })
  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />

          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}

          />
      );
  }
  renderItem = (title, color, icon, route) => {

      return (

        <ListItem
            divider
            leftElement={<Icon name={icon} size={26} color={color} />}
            centerElement={title}
            rightElement="chevron-right"
            onPress={() => this.props.navigator.push(route)}
        />

      );
  }
  onUploadPhoto= () =>  {

      var url =  DevApi + 'profile/add_photo';
      var that = this;
      return fetch(url, {
              method: 'PUT',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              },
              body: JSON.stringify({

                'link_picture': this.state.uploadURL,

              }),
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          if (result.data.status == '200') {
            ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
            that.props.navigator.pop();
            that.setState({ data : result.data });
          }
          else{
          ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
        }
        })
        .catch((error) => { console.error(error); });
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="Pilih Photo"/>
                    {
          (() => {
            switch (this.state.uploadURL) {
              case null:
                return null
              case '':
                return <ActivityIndicator />
              default:
                return (
                  <View>
                    <Image
                      source={{ uri: this.state.uploadURL }}
                      style={ styles.image }
                    />
                  </View>
                )
            }
          })()
        }
        <View style={styles.formContainer}>
          <TouchableOpacity onPress={ () => this._pickImage() }>
            <View style={styles.camera}>
              <Avatar icon="camera-alt" size={100} color={COLOR.teal700}/>
            </View>
        </TouchableOpacity>

            <TouchableOpacity activeOpacity={.5} style={styles.buttonContainer} onPress={this.onUploadPhoto}>
              <Text style={styles.buttonTitle}>
                UPLOAD
              </Text>
            </TouchableOpacity>
        </View>
                   </ScrollView>
            </View>
        );
    }

}

AddProfilePhoto.propTypes = propTypes;

export default AddProfilePhoto;
