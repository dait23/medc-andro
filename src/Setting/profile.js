import { Text, NetInfo, Dimensions, View, RefreshControl, StyleSheet, ScrollView, ToastAndroid, Platform, Image } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import NetPage from '../Net';
import ActionButton from 'react-native-action-button';
import { IconToggle, Card, Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImageSlider from 'react-native-image-slider';
import {Column as Col, Row} from 'react-native-flexbox-grid';

import Iconx from 'react-native-vector-icons/FontAwesome';

import Iconz from 'react-native-vector-icons/Ionicons';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const welcomeImg = require('./../img/welcome.jpg');
const male = require('./../img/male-avatar.png');
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
    kidsbox:{
      alignItems:'center',
      alignSelf:'center',
      justifyContent: "center",
      height: deviceHeight / 3.8,
      marginTop:30,
      marginBottom:20,
    },
    kidsname:{
      fontSize:16,
      fontWeight:'bold',
      alignSelf:'center',
      marginTop:5,
      marginBottom:5,
    },
    kidsfemale:{
      color:COLOR.pink400,
    },
    kidsmale:{
      color:COLOR.medcColor,
    },
    actionButtonIcon: {
      fontSize: 30,
      height: 32,
    color: 'white',
  },
  avatar:{
    width:100,height:100,borderRadius:100,borderWidth:1,borderColor:'#fff',
  }
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class Profile extends Component {
  constructor(props) {
      super(props);

      this.state = {
          selected: [],
          searchText: '',
          data : {},
          refreshing: false,
           isConnected: true,

      };
  }

  componentDidMount() {

      var that = this;
      this.profile();
      NetInfo.isConnected.addEventListener(
     'change',
     this._handleConnectivityChange
     );
     NetInfo.isConnected.fetch().done(
         (isConnected) => { this.setState({isConnected}); }
     );

  }
  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'change',
      this._handleConnectivityChange
  );
}
_handleConnectivityChange = (isConnected) => {
this.setState({
  isConnected,
});
};
  _onRefresh() {
    this.setState({refreshing: true});
    this.profile().then(() => {
      this.setState({refreshing: false});
    });
  }
  profile() {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });

      var url =  DevApi + 'profile';
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              }
        })
        .then(function(response) {
        return response.json();
      }).then(function(result) {
        that.setState({
          data : result.data,
          noreg:result.data.noreg,
          nama: result.data.namapasien,
          gender:result.data.jeniskel,
          email:result.data.email,
          telpon:result.data.telpon,
          alamat:result.data.alamat,
          berat:result.data.berat,
          tinggi:result.data.tinggi,
          picture: result.data.link_picture,
          loading: false
         });
      })
      .catch((error) => { console.error(error); });
  }
  RenderOffline(){

    if(this.state.isConnected == false){

        return(
            <View>
             <NetPage />
            </View>

        );
    }else{

       return(

         <View>
         {this.renderMain()}
         </View>
       )
    }
  }
  renderMain= () =>{

    return(

      <View style={{marginTop:0, backgroundColor:'#f9f9f9'}}>

      <Row size={12} style={styles.Rm} nowarp>
         <Col sm={12} md={6} lg={6}>
            <View style={styles.kidsbox}>
               {this.renderpicture()}
              <Text style={styles.kidsname}>{this.state.nama}</Text>
              <Text style={styles.kidsmale}>{this.state.noreg}</Text>
              <Text style={{alignSelf:'center'}}>{this.state.gender}</Text>
            </View>
         </Col>
      </Row>


          <Subheader text="Biodata"/>
            {this.renderItem(this.state.nama, COLOR.medcColor, 'person')}
            {this.renderItem(this.state.email, COLOR.medcColor,'mail')}
            {this.renderItem(this.state.telpon, COLOR.medcColor, 'phone')}
            {this.renderItem(this.state.alamat, COLOR.medcColor,'home')}
      </View>


    );
  }
  renderpicture(){

    if(this.state.picture == ''){

      return(

          <Image source={male} style={styles.avatar}/>

      );

    }else{

      return(
       <Image source={{ uri: this.state.picture }} style={{width:100,height:100,borderRadius:100,borderWidth:3,borderColor:COLOR.medcColor, alignSelf:'center'}}/>
      );

    }
  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              rightElement="edit"
                onRightElementPress={() => this.props.navigator.push(routes.editprofile)}
          />
      );
  }
  renderItem = (title, color, icon) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={40} color={color}/>}
            centerElement={title}

        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView
                style={styles.container}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh.bind(this)}
                  />
                }
                >
                {this.RenderOffline()}

                </ScrollView>

                <ActionButton buttonColor={COLOR.pink500} bgColor="rgba(0,0,0,0.8)">
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Add Photo" onPress={() => this.props.navigator.push(routes.addphoto)}>
                      <Iconx name="camera" style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                </ActionButton>

            </View>
        );
    }
}

Profile.propTypes = propTypes;

export default Profile;
