import { Text, TextInput, Picker, DatePickerAndroid, TouchableOpacity, View,StyleSheet, ScrollView, ToastAndroid, Platform } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import { ActionButton, Card, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import Iconx from 'react-native-vector-icons/FontAwesome';
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    formContainer:{
      padding:15,
    },
    input:{
      height:40,
      backgroundColor:COLOR.grey100,
      marginBottom:10,
      color:'#000',
      paddingLeft:10
    },
    buttonContainer:{
      backgroundColor:COLOR.medcColor,
      paddingVertical:15,
      marginBottom:20,

    },
    buttonTitle:{
      textAlign:'center',
      color:'#fff',
      fontWeight:'700',

    },
    box:{
      flex:1,
      flexDirection:'row',
      justifyContent: 'space-around',
    },
    pick:{
      height:40,
      marginBottom:10,
      color:'#fff',
      textAlign:'center',
      width:100,
      justifyContent: 'center',
      paddingVertical:10,
    },
    inputPick:{
      height:40,
      backgroundColor:COLOR.grey100,
      marginBottom:10,
      color:'#000',
      paddingLeft:10,
      width:225
    },
    area:{
      height:70,
      backgroundColor:COLOR.grey100,
      marginBottom:10,
      color:'#000',
      paddingLeft:10
    },
});
const Item = Picker.Item;
const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class Form extends Component {
  constructor(props) {
    super(props);
    this.onUpdatePress = this.onUpdatePress.bind(this);
    this.state = {
      selectedItem: undefined,
      jeniskel: 'Jenis Kelamin',
      tgllhr: 'Tanggal Lahir',
      namapasien: '',
      email: '',
      tgllhr: '',
      jeniskel: '',
      alamat: '',
      telpon: '',
      ktp: '',
      berat: '',
      tinggi: '',
      data : {},
      results: {
        items: [],
      },
    };
  }
  componentDidMount() {

      var that = this;
      this.profile();

  }
  profile() {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });

      var url =  DevApi + 'profile';
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              }
        })
        .then(function(response) {
        return response.json();
      }).then(function(result) {
        that.setState({
          data : result.data,
          noreg:result.data.noreg,
          namapasien: result.data.namapasien,
          gender:result.data.jeniskel,
          email:result.data.email,
          telpon:result.data.telpon,
          ktp:result.data.ktp,
          alamat:result.data.alamat,
          berat:result.data.berat,
          tinggi:result.data.tinggi,
          tgllhr:result.data.tgllhr,
          picture: result.data.link_picture,
          loading: false
         });
      })
      .catch((error) => { console.error(error); });
  }

  onValueChange(value: string) {
    this.setState({
      jeniskel: value,
    });
  }
  showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'Text'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'Text'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };
  onUpdatePress() {

      var url =  DevApi + 'profile/update';
      var that = this;
      return fetch(url, {
              method: 'PUT',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              },
              body: JSON.stringify({

                namapasien: this.state.namapasien,
                email: this.state.email,
                tgllhr: this.state.simpleText,
                jeniskel: this.state.jeniskel,
                alamat: this.state.alamat,
                ktp: this.state.ktp,
                telpon: this.state.telpon,
                tinggi: this.state.tinggi,
                berat: this.state.berat,

              }),
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          if (result.data.status == '200') {
            ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
            //this.props.navigator.pop();
            that.setState({ data : result.data });
          }
          else{
          ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
          }
        })
        .catch((error) => { console.error(error); });
    }

    render() {
        return (


                    <View style={styles.formContainer}>
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Nomer register"
                          placeholderTextColor="rgba(0,0,0,0.6)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(noreg) => this.setState({noreg})}
                          returnKeyType="next"
                          ref="editNoreg"
                          editable={false}
                          value={this.state.noreg}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Nama anda"
                          placeholderTextColor="rgba(0,0,0,0.6)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(namapasien) => this.setState({namapasien})}
                          returnKeyType="next"
                          ref="editNama"
                          value={this.state.namapasien}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Email"
                          placeholderTextColor="rgba(0,0,0,0.6)"
                          keyboardType="email-address"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(email) => this.setState({email})}
                          returnKeyType="next"
                          ref="editEmail"
                          value={this.state.email}
                        />
                        <View style={styles.box}>
                        <TextInput
                          style={styles.inputPick}
                          underlineColorAndroid='transparent'
                          placeholder="Tanggal Lahir"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(simpleText) => this.setState({simpleText})}
                          returnKeyType="next"
                          value={this.state.simpleText}
                        />
                          <TouchableOpacity
                            onPress={this.showPicker.bind(this, 'simple', {date: this.state.simpleDate})}>
                            <View style={styles.pick}>
                              <Iconx name="calendar" size={28} color={COLOR.medcColor} style={{alignSelf:'center'}}/>
                            </View>
                          </TouchableOpacity>
                        </View>
                        <Picker
                          mode="modal"
                          selectedValue={this.state.jeniskel}
                          onValueChange={this.onValueChange.bind(this)}
                          style={styles.input}
                        >
                          <Item label="Laki-laki" value="laki-laki" />
                          <Item label="Perempuan" value="perempuan" />
                        </Picker>
                        <TextInput
                          style={styles.area}
                          underlineColorAndroid='transparent'
                          placeholder="Alamat"
                          placeholderTextColor="rgba(0,0,0,0.6)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(alamat) => this.setState({alamat})}
                          returnKeyType="next"
                          value={this.state.alamat}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Nomer telepon"
                          placeholderTextColor="rgba(0,0,0,0.6)"
                          keyboardType="numeric"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(telpon) => this.setState({telpon})}
                          returnKeyType="next"
                          value={this.state.telpon}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Nomer KTP"
                          placeholderTextColor="rgba(0,0,0,0.6)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(ktp) => this.setState({ktp})}
                          returnKeyType="next"
                          value={this.state.ktp}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Tinggi (Cm)"
                          placeholderTextColor="rgba(0,0,0,0.6)"
                          keyboardType="numeric"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(tinggi) => this.setState({tinggi})}
                          returnKeyType="next"
                          value={this.state.tinggi}
                        />
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Berat (Kg)"
                          placeholderTextColor="rgba(0,0,0,0.6)"
                          keyboardType="numeric"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(berat) => this.setState({berat})}
                          returnKeyType="done"
                          value={this.state.berat}
                        />
                        <TouchableOpacity activeOpacity={.5} style={styles.buttonContainer} onPress={this.onUpdatePress}>
                          <Text style={styles.buttonTitle}>
                            UPDATE
                          </Text>
                        </TouchableOpacity>
                    </View>
        );
    }

}

Form.propTypes = propTypes;
export default Form;
