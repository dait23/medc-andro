import { Text, TextInput, Picker, DatePickerAndroid, TouchableOpacity, View,StyleSheet, ScrollView, ToastAndroid, Platform } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import { ActionButton, Card, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconM from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconx from 'react-native-vector-icons/FontAwesome';
import Form from './form';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    formContainer:{
      padding:15,
    },
    input:{
      height:40,
      backgroundColor:COLOR.grey300,
      marginBottom:10,
      color:'#000',
      paddingLeft:10
    },
    buttonContainer:{
      backgroundColor:COLOR.medcColor,
      paddingVertical:15,
      marginBottom:20,

    },
    buttonTitle:{
      textAlign:'center',
      color:'#fff',
      fontWeight:'700',

    },

});



const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class EditProfile extends Component {
  constructor(props) {
      super(props);

      this.state = {
          selected: [],
          searchText: '',


      };
  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />

          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}

          />
      );
  }
  renderItem = (title, color, icon, route) => {

      return (

        <ListItem
            divider
            leftElement={<Icon name={icon} size={26} color={color} />}
            centerElement={title}
            rightElement="chevron-right"
            onPress={() => this.props.navigator.push(route)}
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="Biodata"/>
                      <Form />
                   </ScrollView>
            </View>
        );
    }

}

EditProfile.propTypes = propTypes;

export default EditProfile;
