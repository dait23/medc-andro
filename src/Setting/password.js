import { Text, TextInput, View, StyleSheet, ScrollView, ToastAndroid, Platform, Alert,TouchableOpacity  } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import { ActionButton, Card, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import Icon from 'react-native-vector-icons/FontAwesome';
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    formContainer:{
      padding:20,
    },
    input:{
      height:40,
      backgroundColor:COLOR.grey300,
      marginBottom:10,
      color:'#000',
      paddingLeft:10
    },
    buttonContainer:{
      backgroundColor:COLOR.medcColor,
      paddingVertical:15,
      marginBottom:20,

    },
    buttonTitle:{
      textAlign:'center',
      color:'#fff',
      fontWeight:'700',

    },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class Pass extends Component {
  constructor(props) {
      super(props);
      this.onUpdatePress = this.onUpdatePress.bind(this);
      this.state = {
          selected: [],
          searchText: '',
          password: "",

      };
  }
  onUpdatePress() {

      var url =  DevApi + 'profile/update_password';
      var that = this;
      return fetch(url, {
              method: 'PUT',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              },
              body: JSON.stringify({

                'password': this.state.password


              }),
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          if (result.data.status == '200') {
            ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
            that.props.navigator.pop();
            that.setState({ data : result.data });
          }
          else{
          ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
          }
        })
        .catch((error) => { console.error(error); });
    }

  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}

          />
      );
  }
  renderItem = (title, color, icon, route) => {

      return (

        <ListItem
            divider
            leftElement={<Icon name={icon} size={26} color={color} />}
            centerElement={title}
            rightElement="chevron-right"
            onPress={() => this.props.navigator.push(route)}
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="Masukan Password baru anda"/>

                    <View style={styles.formContainer}>
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Password"
                          placeholderTextColor="rgba(0,0,0,0.6)"
                          keyboardType="default"
                          autoCapitalize="none"
                          secureTextEntry={true}
                          autoCorrect={false}
                          onChangeText={(password) => this.setState({password})}
                          returnKeyType="done"
                          value={this.state.password}
                        />
                        <TouchableOpacity activeOpacity={.9} style={styles.buttonContainer} onPress={this.onUpdatePress}>
                          <Text style={styles.buttonTitle}>
                            UPDATE
                          </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

Pass.propTypes = propTypes;

export default Pass;
