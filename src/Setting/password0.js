import { Text, View, StyleSheet, ScrollView, ToastAndroid, Platform, Alert } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import { ActionButton, Card, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import Icon from 'react-native-vector-icons/FontAwesome';
var {GiftedForm} = require('react-native-gifted-form');
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class Pass extends Component {
  constructor(props) {
      super(props);
      this.onUpdatePress = this.onUpdatePress.bind(this);
      this.state = {
          selected: [],
          searchText: '',
          password: "",

      };
  }
  onUpdatePress() {
      // Set loading to true to display a Spinner
    //  this.setState({
        //  loading: true
      //});

      var url = 'http://rest.medclabs.com/index.php/profile/update_password';
      var that = this;
      return fetch(url, {
              method: 'PUT',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              },
              body: JSON.stringify({

                'password': this.state.password


              }),
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {


              Alert.alert(
                    result.data.message
               )

          that.setState({ data : result.data });
        })
        .catch((error) => { console.error(error); });
    }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}

          />
      );
  }
  renderItem = (title, color, icon, route) => {

      return (

        <ListItem
            divider
            leftElement={<Icon name={icon} size={26} color={color} />}
            centerElement={title}
            rightElement="chevron-right"
            onPress={() => this.props.navigator.push(route)}
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="Masukan Password baru anda"/>

                    <View style={{marginLeft:15, marginRight:15}}>

                    <GiftedForm
                      formName='resetForm'

                      validators={{


                        password: {
                          title: 'Password',
                          validate: [{
                            validator: 'isLength',
                            arguments: [6, 16],
                            message: '{TITLE} harus antara {ARGS[0]} dan {ARGS[1]} karakter',
                          }]
                        },



                  }}>

                    <GiftedForm.TextInputWidget
                      name='password'
                      image={<Icon name="lock" size={25} color="#ccc" />}
                      placeholder='password'
                      clearButtonMode='while-editing'
                      underlineColorAndroid='transparent'
                      value={this.state.password}
                      secureTextEntry
                      onChangeText={(password) => this.setState({password})}

                    />
                    <GiftedForm.SubmitWidget
                      title='Update Password'
                      widgetStyles={{
                        submitButton: {
                          backgroundColor: COLOR.medcColor,
                        }
                      }}
                      onSubmit={(isValid, values, validationResults, postSubmit = null, modalNavigator = null) => {
                        if (isValid === true) {

                          onUpdatePress();
                        }
                      }}

                    />


                    <GiftedForm.HiddenWidget name='tos' value={true} />

                  </GiftedForm>
                  </View>

                </ScrollView>
            </View>
        );
    }
}

Pass.propTypes = propTypes;

export default Pass;
