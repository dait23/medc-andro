
import React, { Component, PropTypes } from 'react';
import {
ToastAndroid,
ScrollView,
StyleSheet,
Text,
View,
Image,
Dimensions,
TextInput,
Button,
Alert,
TouchableOpacity,
AsyncStorage,
} from 'react-native';

import routes from '../routes';
//import Color from 'color';
import Container from '../Container';

import {DevApi, MainApi} from '../Api';
// components
import {  COLOR, ThemeProvider} from '../react-native-material-ui/src';

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    onPress: PropTypes.func
};
const { width, height } = Dimensions.get("window");

const background = require("../img/launchscreen-bg.png");
const backIcon = require("./back.png");
const personIcon = require("./signup_person.png");
const lockIcon = require("./signup_lock.png");
const emailIcon = require("./signup_email.png");
const birthdayIcon = require("./signup_birthday.png");
const phoneIcon = require("./signup_phone.png");


let styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bg: {
    flex:1,
    paddingTop: 20,
    width: null,
    height: null
  },
  headerContainer: {
    flex: 1,
  },
  inputsContainer: {
    flex: 3,
    marginTop: 30,
  },
  footerContainer: {
    flex: 1
  },
  headerIconView: {
    marginLeft: 10,
    backgroundColor: 'transparent'
  },
  headerBackButtonView: {
    width: 25,
    height: 25,
  },
  backButtonIcon: {
    width: 25,
    height: 25
  },
  headerTitleView: {
    backgroundColor: 'transparent',
    marginTop: 25,
    marginLeft: 25,
  },
  titleViewText: {
    fontSize: 26,
    color: '#fff',
  },
  inputs: {
    paddingVertical: 20,
  },
  inputContainer: {
    borderWidth: 1,
    borderBottomColor: '#CCC',
    borderColor: 'transparent',
    flexDirection: 'row',
    height: 75,
  },
  iconContainer: {
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputIcon: {
    width: 30,
    height: 30,
  },
  input: {
    flex: 1,
    fontSize: 20,
  },
  signup: {
    backgroundColor: '#FF3366',
    paddingVertical: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15,
    marginTop:50,
  },
  signin: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    marginBottom:20,
  },
  greyFont: {
    color: '#D8D8D8'
  },
  whiteFont: {
    color: '#FFF',
    fontSize: 18,
  }
});

class Register extends Component {

  constructor(props) {
    super(props);
    this.onSignUpPress = this.onSignUpPress.bind(this);
    this.state = {
      namapasien: "",
      email: "",
      telpon: "",
      password: "",
      error: "",
      data : {}
    };
  }
  focusChangeField = (focusField) => {
    this.refs[focusField].focus();
  }
  componentWillMount() {
    const value = AsyncStorage.getItem('@userDetails');
    if (value !== null){
      console.log(value);
    } else {
      console.log("no");
    }
  }
  onSignUpPress() {

      var url =  DevApi + 'auth/register';
      var that = this;
      return fetch(url, {
              method: 'POST',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              },
              body: JSON.stringify({

                'namapasien' : this.state.namapasien,
                'telpon' : this.state.telpon,
                'email' : this.state.email,
                'password': this.state.password

              }),
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
            if (result.data.status == '200') {
              //AsyncStorage.setItem('@userDetails', JSON.stringify(userDetails));
              ToastAndroid.show(result.data.message, ToastAndroid.SHORT),
              that.props.navigator.pop();

            }else{
              ToastAndroid.show(result.data.message, ToastAndroid.SHORT);

            }


          that.setState({ data : result.data, loginder: false  });
        })
        .catch((error) => { console.error(error); });
    }

  render() {
    return (
      <ScrollView
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="interactive"
      >
      <View style={styles.container}>
        <Image
          source={background}
          style={styles.bg}
        >
          <View style={styles.headerContainer}>

            <View style={styles.headerIconView}>
              <TouchableOpacity style={styles.headerBackButtonView} onPress={() => this.props.navigator.pop()}>
                <Image
                  source={backIcon}
                  style={styles.backButtonIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>

            <View style={styles.headerTitleView}>
              <Text style={styles.titleViewText}>Register</Text>
            </View>

          </View>

          <View style={styles.inputsContainer}>

            <View style={styles.inputContainer}>
              <View style={styles.iconContainer}>
                <Image
                  source={personIcon}
                  style={styles.inputIcon}
                  resizeMode="contain"
                />
              </View>
              <TextInput
                style={[styles.input, styles.whiteFont]}
                placeholder="Name Anda"
                returnKeyType="next"
                placeholderTextColor="#FFF"
                keyboardType="default"
                ref="signUpName"
                underlineColorAndroid='transparent'
                onChangeText={(namapasien) => this.setState({namapasien})}
                onSubmitEditing={() => this.focusChangeField('signUpEmail')}
                value={this.state.namapasien}
              />
            </View>

            <View style={styles.inputContainer}>
              <View style={styles.iconContainer}>
                <Image
                  source={emailIcon}
                  style={styles.inputIcon}
                  resizeMode="contain"
                />
              </View>
              <TextInput
                style={[styles.input, styles.whiteFont]}
                placeholder="Email Anda"
                placeholderTextColor="#FFF"
                keyboardType="email-address"
                returnKeyType="next"
                ref="signUpEmail"
                underlineColorAndroid='transparent'
                onChangeText={(email) => this.setState({email})}
                onSubmitEditing={() => this.focusChangeField('signUpPassword')}
                value={this.state.email}

              />
            </View>

            <View style={styles.inputContainer}>
              <View style={styles.iconContainer}>
                <Image
                  source={lockIcon}
                  style={styles.inputIcon}
                  resizeMode="contain"
                />
              </View>
              <TextInput
                secureTextEntry={true}
                style={[styles.input, styles.whiteFont]}
                placeholder="Password"
                placeholderTextColor="#FFF"
                underlineColorAndroid='transparent'
                keyboardType="default"
                ref="signUpPassword"
                returnKeyType="next"
                onChangeText={(password) => this.setState({password})}
                onSubmitEditing={() => this.focusChangeField('signUpTelpon')}
                value={this.state.password}
              />
            </View>
            <View style={styles.inputContainer}>
              <View style={styles.iconContainer}>
                <Image
                  source={phoneIcon}
                  style={styles.inputIcon}
                  resizeMode="contain"
                />
              </View>
              <TextInput
                style={[styles.input, styles.whiteFont]}
                placeholder="Nomer Hp Anda"
                placeholderTextColor="#FFF"
                underlineColorAndroid='transparent'
                keyboardType="numeric"
                ref="signUpTelpon"
                returnKeyType="done"
                onChangeText={(telpon) => this.setState({telpon})}
                value={this.state.telpon}
                onSubmitEditing={this.onSignUpPress}
              />
            </View>


          </View>

          <View style={styles.footerContainer}>

            <TouchableOpacity onPress={this.onSignUpPress}>
              <View style={styles.signup}>
                <Text style={styles.whiteFont}>Register</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() =>this.props.navigator.pop()}>
              <View style={styles.signin}>
                <Text style={styles.greyFont}>Sudah punya akun?<Text style={styles.whiteFont}> Sign In</Text></Text>
              </View>
            </TouchableOpacity>
          </View>
        </Image>
      </View>

      </ScrollView>
    );
  }

}


Register.propTypes = propTypes;

export default Register;
