import { AppRegistry } from 'react-native';
import React, { Component } from 'react';

import App from './App/App.react';
import SplashScreen from 'react-native-splash-screen';


export default function index() {
    class Root extends Component {
      componentDidMount() {
            SplashScreen.hide();
        }
        render() {
            return (
                <App />
            );
        }
    }

    AppRegistry.registerComponent('MedclabsApp', () => Root);
}
