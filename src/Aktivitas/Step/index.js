import {TextInput, Text, Dimensions, View, StyleSheet, ScrollView, ToastAndroid, Platform, Image, ListView, TouchableHighlight} from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../../routes';
import { ActionButton, IconToggle, Card, Avatar, Subheader, Toolbar, COLOR, ThemeProvider} from '../../react-native-material-ui/src';

import { decorator as sensors} from 'react-native-sensors';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
      flex: 1,
  },

});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class StepCounter extends Component {
  // Normal RxJS functions

  renderToolbar = () => {

    return (
        <Toolbar
            key="toolbar"
            leftElement="arrow-back"
            onLeftElementPress={() => this.props.navigator.pop()}
            centerElement={this.props.route.title}
        />
    );

  }
  render() {
    const {
      Accelerometer
    } = this.props;

    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        <Text>
          Acceleration has value: {Accelerometer}
        </Text>
      </View>
    );
  }
}



StepCounter.propTypes = propTypes;
export default StepCounter;
