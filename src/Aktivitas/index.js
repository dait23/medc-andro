import { Text, Dimensions, View, StyleSheet, ScrollView, ToastAndroid, Platform, Image, TouchableOpacity } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import { Avatar, IconToggle, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImageSlider from 'react-native-image-slider';
import {Column as Col, Row} from 'react-native-flexbox-grid';

import Iconx from 'react-native-vector-icons/Ionicons';
import Iconz from 'react-native-vector-icons/FontAwesome';
import ActionButton from 'react-native-action-button';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const welcomeImg = require('./../img/welcome.jpg');

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
    actionButtonIcon: {
      fontSize: 30,
      height: 32,
    color: 'white',
  },
  icon:{
     width:40,
     height:40,
     borderRadius:20,
  }
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};
class Aktivitas extends Component {
  constructor(props) {
      super(props);

      this.state = {
          selected: [],
          searchText: '',

      };
  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              searchable={{
                  autoFocus: true,
                  placeholder: 'Search',
                  onChangeText: value => this.setState({ searchText: value }),
                  onSearchClosed: () => this.setState({ searchText: '' }),
              }}
          />
      );
  }
  renderItem = (title, color, icon) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={40} color={color}/>}
            centerElement={title}
            rightElement="chevron-right"
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>




                    <View style={{marginTop:0, backgroundColor:'#f9f9f9'}}>


                        <Subheader text="Aktivitas List"/>

                    {this.renderItem('Body Index', COLOR.teal700,'fitness-center')}
                    {this.renderItem('My Stopwatch', COLOR.pink500,'alarm-on')}
                    {this.renderItem('Detak Jantung', COLOR.teal700,'directions-run')}



                    </View>

                </ScrollView>
                <ActionButton buttonColor={COLOR.pink500} bgColor="rgba(0,0,0,0.8)">
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Stopwatch" onPress={() => this.props.navigator.push(routes.stopwatch)}>
                      <Iconx name="md-alarm" style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                    <ActionButton.Item  buttonColor={COLOR.medcColor} style={styles.icon} title="Body Index" onPress={() => this.props.navigator.push(routes.bodyindex)}>
                      <Iconz name="user-times" style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                    <ActionButton.Item  buttonColor={COLOR.medcColor} style={styles.icon} title="Heart Monitor" onPress={() => this.props.navigator.push(routes.heart)}>
                      <Iconz name="heartbeat" style={styles.actionButtonIcon} />
                    </ActionButton.Item>

                </ActionButton>
            </View>
        );
    }
}

Aktivitas.propTypes = propTypes;

export default Aktivitas;
