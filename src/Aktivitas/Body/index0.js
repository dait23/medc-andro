import {TextInput, Text, TouchableOpacity, Dimensions, View, StyleSheet, ScrollView, ToastAndroid, Platform, Image, ListView, TouchableHighlight} from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../../routes';
import { ActionButton, IconToggle, Card, Avatar, Subheader, Toolbar, COLOR, ThemeProvider} from '../../react-native-material-ui/src';
import Moment from 'moment';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
      flex: 1,
  },
    containerx: {
        backgroundColor: COLOR.cyan500,
    },
    title: {
      fontSize: 22,
      textAlign: 'center',
      color: '#fff',
      marginBottom:20,
      fontWeight:'700'
    },
    instructions: {
      textAlign: 'center',
      color: '#fff',
      marginBottom: 5,
    },
    inputs: {
      width: 100,
      height:40,
      borderRadius:5,
      fontSize: 14,
      paddingLeft: 10,
      backgroundColor: '#fff',
      margin: 5,
      alignSelf: 'center',
      textAlign:'center'
    },
    button: {
      backgroundColor: COLOR.medcColor,
      color: '#fff',
      fontSize: 14,
      padding: 10,
      margin: 10,
      width: 100,
      height:100,
      borderRadius:100,
      textAlign: 'center',
      textAlignVertical: 'center',
      alignSelf: 'center',
    },
    result: {
      width:80,
      height:80,
      borderRadius:80,
      borderWidth:3,
      borderColor:'#fff',
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf:'center',
      marginTop:30,
      marginBottom:20
    },
    resultText:{

      fontSize: 30,
      color: '#fff',
      fontWeight:'700'
    },
    hasil:{

      fontSize:14,
      color: '#fff',
      fontWeight:'500',
      alignSelf:'center'
    },
    label: {
      fontSize: 16,
      color: '#fff',
      margin: 5
    },
    today: {
      borderWidth:2,
      height:40,
      width:150,
      borderRadius:20,
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf:'center',
      borderColor:'#fff',
      marginTop:30,
      marginBottom:30
    },
    todayText:{
      color:'#fff',
    },
    box:{
      flex:1,
      flexDirection:'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
});


const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

const BodyIndex = React.createClass( {
  renderToolbar: function() {

    return (
        <Toolbar
            key="toolbar"
            leftElement="arrow-back"
            onLeftElementPress={() => this.props.navigator.pop()}
            centerElement={this.props.route.title}
            rightElement={{
                menu: { labels: ['Info']}
            }}
        />
    );

  },
  getInitialState: function() {
    return {
      showResult: false,
      weight: 50,
      height: 180,
      BMI: 0,
      message: null
    }
  },
  calculateBMI: function() {
    weight = this.state.weight
    height = this.state.height
    if (weight.length > 0 && height.length > 0) {
      BMI = weight/(Math.pow((height/100), 2))
      this.setState({
        showResult: true,
        BMI: Math.round(BMI),
        message: null
      })
    } else {
      this.setState({
        message: "please fill all the fields",
        showResult: false
      })
    }
  },
  renderHasil: function(){
    if(this.state.showResult == true && this.state.BMI <='19'){

      return(
           <View>
            <Text style={styles.hasil}>
               Berat Anda Tidak Normal / Kurus
            </Text>
          </View>
      )
    }else if (this.state.showResult == true && this.state.BMI <='25') {
      return(
           <View>
            <Text style={styles.hasil}>
               Berat Anda  Normal
            </Text>
          </View>
      )
    }
    else if (this.state.showResult == true && this.state.BMI >'25'){
      return(
           <View>
            <Text style={styles.hasil}>
               Berat Tidak Normal / Kegemukan
            </Text>
          </View>
    )
    }
  },
  renderResult: function(){

    if (this.state.showResult == true){

        return(
          <View style={styles.result}>
            <Text style={styles.resultText}>
            {this.state.BMI}
            </Text>
            </View>
        )
    }else{
        {this.state.message}
    }


  },

  render: function() {
    const dateTime= new Date();
    Moment.locale('id') //For Turkey
    const formattedDT = Moment(dateTime).format('LL') //2 May
    return (

      <View style={styles.container}>
          {this.renderToolbar()}
          <ScrollView style={styles.containerx}>
          <View style={styles.today}>
          <Text style={styles.todayText}>
             {formattedDT}
          </Text>
          </View>
      <Text
        style={styles.title}>
      Berat Ideal
      </Text>
      <View style={styles.box}>

        <View>
        <TextInput
         placeholder="Berat Kg"
         placeholderTextColor="rgba(0,0,0,0.2)"
         onChangeText={(text) => this.setState({weight: text}) }
         keyboardType="numeric"
        returnKeyType="next"
         underlineColorAndroid='transparent'
         style={styles.inputs}/>

        <TextInput
         placeholder="Tinggi Cm"
         placeholderTextColor="rgba(0,0,0,0.2)"
         onChangeText={(text) => this.setState({height: text}) }
         keyboardType="numeric"
        returnKeyType="done"
         underlineColorAndroid='transparent'
         style={styles.inputs}/>

        </View>
        <View>

        <TouchableOpacity activeOpacity={.5}
          onPress={this.calculateBMI}>
          <Text style={styles.button}>Hitung</Text>
        </TouchableOpacity>
        </View>

      </View>



      {this.renderResult()}
       {this.renderHasil()}
          </ScrollView>
      </View>

    )
  }
})



BodyIndex.propTypes = propTypes;

export default BodyIndex;
