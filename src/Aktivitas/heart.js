import {
  TouchableNativeFeedback,
  DeviceEventEmitter, Text, Dimensions, View, StyleSheet, ScrollView, ToastAndroid, Platform, Image, TouchableOpacity } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {  Card, Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImageSlider from 'react-native-image-slider';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import Iconx from 'react-native-vector-icons/FontAwesome';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Subscribable from 'Subscribable';
import Moment from 'moment';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const welcomeImg = require('./../img/welcome.jpg');

const dateTime= new Date();
Moment.locale('id') //For Turkey
const formattedDT = Moment(dateTime).format('LL') //2 May

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
    kidsbox:{
      alignItems:'center',
      alignSelf:'center',
      justifyContent: "center",
      height: deviceHeight / 3.8,
      marginTop:30,
      marginBottom:20,
    },
    kidsname:{
      fontSize:16,
      fontWeight:'bold',
      alignSelf:'center',
      marginTop:5,
    },
    kidsfemale:{
      color:COLOR.pink400,
    },
    kidsmale:{
      color:COLOR.medcColor,
    },
    actionButtonIcon: {
    fontSize: 30,
    height: 32,
    color: 'white',
  },
  today: {
    borderWidth:2,
    height:40,
    width:150,
    borderRadius:20,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf:'center',
    borderColor:COLOR.pink500,
    marginTop:30
  },
  todayText:{
    color:COLOR.pink500
  },
  heartrate: {
    fontSize: 18,
    textAlign: 'center',
    margin: 30,
    marginBottom: 0,
  },
  heartrateValue: {
    fontSize: 40,
    color: COLOR.pink400,
    textAlign:'center',

  },
  heartBox:{
      justifyContent: 'center',
      width:100,
      height:100,
      borderRadius:100,
      marginTop:30,
      borderWidth:3,
      borderColor:COLOR.pink400,

  },
  containerBox: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};
class HeartRate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      msg: '0',
      selected: [],
      searchText: '',
    };
  }
  addListenerOn() {
    return Subscribable.Mixin.addListenerOn.apply(this, arguments);
  }
  componentWillMount() {
    Subscribable.Mixin.componentWillMount.apply(this, arguments);
  }
  _handleHeartRateCange(msg) {
    console.log('handle heart rate', msg);
    this.setState({msg});
  }
  componentDidMount() {
      this.addListenerOn(DeviceEventEmitter,
                         'heartrateChanged',
                         (msg) => { this._handleHeartRateCange(msg) });
  }
  componentWillUnmount() {
    Subscribable.Mixin.componentWillUnmount.apply(this, arguments);
  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              
          />
      );
  }
  renderHasil(){

    if(this.state.msg == ''){

      return(

        <Text>
        0
        </Text>
      );

    }
    else{
      return(

        <Text>
        {this.state.msg}
        </Text>


      );
    }

   }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <View style={{marginTop:0,}}>

                    <View style={styles.today}>
                      <Text style={styles.todayText}>
                         {formattedDT}
                      </Text>
                    </View>
                    <Iconx name="heartbeat" color={COLOR.pink500} style={{fontSize:70,alignSelf:'center',marginTop:30,marginBottom:0}}/>
                    <View style={styles.containerBox}>
                      <Text style={styles.heartrate}>
                        Heart rate
                      </Text>
                      <View style={styles.heartBox}>
                      <Text style={styles.heartrateValue}>
                        {this.renderHasil()}
                      </Text>
                      </View>
                    </View>
                    </View>

                </ScrollView>


            </View>
        );
    }
}

HeartRate.propTypes = propTypes;

export default HeartRate;
