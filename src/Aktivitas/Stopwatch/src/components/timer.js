import React, { Component } from 'react';
import { Text, View } from 'react-native';
var formatTime = require('minutes-seconds-milliseconds');

class Timer extends Component {
    render() {
        return (
            <View style= {this.props.style.timer}>
                <Text style= {this.props.style.timerText}>
                    {formatTime(this.props.getCurrentState().timeElapsed)}
                </Text>
            </View>
        );
    }
}

module.exports = Timer;
