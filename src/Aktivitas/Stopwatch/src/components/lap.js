import React, { Component } from 'react';
import { Text, View , ListView, ScrollView } from 'react-native';
var formatTime = require('minutes-seconds-milliseconds');

var Lap = React.createClass({
    render: function() {
        return (
          <View style= {this.props.style.footer}>
            {this.getLaps(this.props.getCurrentState, this.props.style)}
          </View>
        );
    },
    getLaps: function(getCurrentState, style) {
        return   <View>
        <ScrollView>
          <ListView
            dataSource = {getCurrentState().dataSource}
            enableEmptySections = {true}
            renderRow  = {
              (rowData, sec, index) =>
              <View style = {[style.lap,this.border('#00bcd4')]}>
                <Text style = {style.lapText}>
                  Lap {parseInt(index)+1}
                </Text>
                <Text style = {style.lapText}> {formatTime(rowData)}</Text>
              </View>
            }
          />
          </ScrollView>
        </View>

    },
    border: function(color) {
        return {
            borderColor: color,
            borderWidth: 2
        }
    }
})

module.exports = Lap;
