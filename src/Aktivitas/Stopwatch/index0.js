import { Text, Dimensions, View, StyleSheet, ScrollView, ToastAndroid, Platform, Image, ListView } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../../routes';
import { ActionButton, IconToggle, Card, Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../../react-native-material-ui/src';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
});

//import Timer from './src/components/timer';
//import ButtonBar  from './src/components/buttonbar';
//import Lap from './src/components/lap';
import style from './src/styles/common';

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};
class Stopwatch extends Component {
  getInitialState() {
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      return {
          timeElapsed : null,
          running     : false,
          startTime   : null,
          lap         : [],
          dataSource  : ds.cloneWithRows([])
      }
  }
  updateState(state) {
      this.setState(state);
  }
  getCurrentState() {
      return this.state;
  }
  renderToolbar = () => {
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              searchable={{
                  autoFocus: true,
                  placeholder: 'Search',
                  onChangeText: value => this.setState({ searchText: value }),
                  onSearchClosed: () => this.setState({ searchText: '' }),
              }}
          />
      );
  }

    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                <View style={style.container}>
                  <View style = {style.header}>


                  </View>

                

                </View>

                </ScrollView>


            </View>
        );
    }
}

Stopwatch.propTypes = propTypes;

export default Stopwatch;
