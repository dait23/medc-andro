import { Text, Dimensions, View, StyleSheet, ScrollView, ToastAndroid, Platform, Image, ListView } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../../routes';
import { ActionButton, IconToggle, Card, Avatar, Subheader, Toolbar, COLOR, ThemeProvider} from '../../react-native-material-ui/src';


const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
    today: {
      borderWidth:2,
      height:40,
      width:150,
      borderRadius:20,
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf:'center',
      borderColor:'#00bcd4',
      marginTop:30
    },
    todayText:{
      color:'#00bcd4'
    }
});

import Timer from './src/components/timer';
import ButtonBar  from './src/components/buttonbar';
import Lap from './src/components/lap';
import style from './src/styles/common';
import Moment from 'moment';
const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

const dateTime= new Date();
Moment.locale('id') //For Turkey
const formattedDT = Moment(dateTime).format('LL') //2 May

var Stopwatch = React.createClass({

  renderToolbar: function() {

    return (
        <Toolbar
            key="toolbar"
            leftElement="arrow-back"
            onLeftElementPress={() => this.props.navigator.pop()}
            centerElement={this.props.route.title}
            
        />
    );

  },
    getInitialState: function() {
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        return {
            timeElapsed : null,
            running     : false,
            startTime   : null,
            lap         : [],
            dataSource  : ds.cloneWithRows([])
        }
    },
    updateState: function(state) {
        this.setState(state);
    },
    getCurrentState: function() {
        return this.state;
    },
    render: function() {
        return (

          <View style={styles.container}>
              {this.renderToolbar()}

                    <View style={style.container}>

                    <View style={styles.today}>
                    <Text style={styles.todayText}>
                       {formattedDT}
                    </Text>
                    </View>

                    <View style = {style.header}>
                        <Timer
                            style           = {style}
                            getCurrentState = {this.getCurrentState}>
                        </Timer>
                        <ButtonBar
                            style           = {style}
                            updateState     = {this.updateState}
                            getCurrentState = {this.getCurrentState}>
                        </ButtonBar>
                    </View>

                    <Lap
                        style           = {style}
                        updateState     = {this.updateState}
                        getCurrentState = {this.getCurrentState}>
                    </Lap>

                    </View>

              </View>
        );
    }
})


Stopwatch.propTypes = propTypes;

export default Stopwatch;
