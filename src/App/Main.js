import React, { Component, PropTypes } from 'react';
import {Alert,  TouchableOpacity,  NetInfo, RefreshControl, Dimensions, ToastAndroid, ScrollView, Platform, Text, View, StyleSheet, Image } from 'react-native';
import Iconx from 'react-native-vector-icons/MaterialIcons';
import Iconz from 'react-native-vector-icons/FontAwesome';
import routes from '../routes';
//import Color from 'color';
import Container from '../Container';
import NetPage from '../Net';
import Media from '../Media';
import {DevApi, MainApi} from '../Api';
//import ControlPanel from '../Drawer/ControlPanel'

//import DrawerSpec from '../Drawer';
//import Drawer from 'react-native-drawer'
import {Column as Col, Row} from 'react-native-flexbox-grid';
import ActionButton from 'react-native-action-button';
import Share, {ShareSheet, Button} from 'react-native-share';
import Communications from 'react-native-communications';
// components
import { Avatar, Card, ListItem, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
//import { COLOR, ThemeProvider } from '../react-native-material-ui';
import ImageSlider from 'react-native-image-slider';

const welcomeImg = require('./../img/welcome.jpg');
const welcomeImg2 = require('./../img/welcome2.jpg');
const welcomeImg3 = require('./../img/welcome3.jpg');
const male = require('./../img/male-avatar.png');
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
    textContainer: {
        paddingHorizontal: 16,
        paddingBottom: 16,
        paddingTop: 16,
        paddingBottom: 16
    },
    fullImage: {
        alignSelf: 'stretch'
    },
    avatarContainer: {
        paddingHorizontal:20,
        paddingVertical:20,
        flex: 1,
        justifyContent: 'center',

    },
    avatarColor:{
      color: '#000',
    },
    Rm:{
      marginBottom:20,
    },
    xxx:{
      width: deviceWidth / 2.1,
      height: deviceHeight / 4,
      alignItems: "center",
      justifyContent: "center",
    },
    tengah:{
      alignSelf: "center",
      fontSize:40
    },
    actionButtonIcon: {
      fontSize: 30,
      height: 32,
    color: 'white',
  },
  icon:{
     width:40,
     height:40,
     borderRadius:20,
  },
  avatar:{
    width:40,height:40,borderRadius:40,marginRight:10,borderWidth:1,borderColor:'#fff',
  }
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    onPress: PropTypes.func,
};

export default class Main extends Component {

    constructor(props) {
        super(props);
        this.profile = this.profile.bind(this)
        this.state = {
            selected: [],
            connectionInfoHistory: [],
             connectionInfo: null,
             isConnected: true,
            searchText: '',
            visible: false,
            refreshing: false,
            data : {}
        };
    }

    componentDidMount() {

        var that = this;
        this.profile();
        NetInfo.isConnected.addEventListener(
       'change',
       this._handleConnectivityChange
       );
       NetInfo.isConnected.fetch().done(
           (isConnected) => { this.setState({isConnected}); }
       );

    }
      componentWillUnmount() {
        NetInfo.isConnected.removeEventListener(
          'change',
          this._handleConnectivityChange
      );
    }
    _onRefresh() {
      this.setState({refreshing: true});
      this.profile().then(() => {
        this.setState({refreshing: false});
      });
    }
    _handleConnectivityChange = (isConnected) => {
    this.setState({
      isConnected,
    });
  };
    profile() {
        // Set loading to true to display a Spinner
        this.setState({
            loading: true
        });

        var url =  DevApi + 'profile';
        var that = this;
        return fetch(url, {
                method: 'GET',
                headers: {
                  'Client-Service': 'mobile-client',
                  'Content-Type': 'application/json',
                  'Auth-key': 'medcrestapi'
                }
          })
          .then(function(response) {
          return response.json();
        }).then(function(result) {
          that.setState({
            data : result.data,
            picture: result.data.link_picture,
            loading: false
           });
        })
        .catch((error) => { console.error(error); });
    }
    renderpicture(){

      if(this.state.picture == ''){

        return(
          <TouchableOpacity activeOpacity={.9}  onPress={() => this.props.navigator.push(routes.profile)}>
           <Image source={male} style={styles.avatar}/>
            </TouchableOpacity>
        );

      }else{

        return(
         <TouchableOpacity activeOpacity={.9}  onPress={() => this.props.navigator.push(routes.profile)}>
           <Image source={{ uri: this.state.picture }} style={styles.avatar}/>
         </TouchableOpacity>
        );

      }
    }

    RenderOffline(){

      if(this.state.isConnected == false){

          return(
              <View>
               <NetPage />
              </View>

          );
      }else{

         return(

           <View>
           {this.renderMain()}
           </View>
         )
      }
    }

    renderMain= () =>{

       return(

          <View style={{marginTop:30, paddingBottom:50}}>
          <Row size={12} style={styles.Rm}>
            <Col sm={6} md={4} lg={3}>
            <View style={styles.xxx}>
              <TouchableOpacity activeOpacity={.9}  onPress={() => this.props.navigator.push(routes.medis)}>
                 <Avatar icon="local-hospital" size={105} color={COLOR.teal700}/>
                 <Text style={{alignSelf:'center', marginTop:5}}>
                  Medis / Labs
                 </Text>
              </TouchableOpacity>
            </View>
            </Col>
            <Col sm={6} md={4} lg={3}>
              <View style={styles.xxx}>
              <TouchableOpacity activeOpacity={.9}  onPress={() => this.props.navigator.push(routes.aktivitas)}>
                <Avatar icon="fitness-center" size={105} color={COLOR.tealA700}/>
                <Text style={{alignSelf:'center', marginTop:5}}>
                 Aktivitas
                </Text>
               </TouchableOpacity>
              </View>
            </Col>
          </Row>
           <Row size={12} style={styles.Rm}>
            <Col sm={6} md={4} lg={3}>
              <View style={styles.xxx}>
              <TouchableOpacity activeOpacity={.9}  onPress={() => this.props.navigator.push(routes.wanita)}>
              <Avatar  icon="pregnant-woman" size={105} color={COLOR.pink500}/>
              <Text style={{alignSelf:'center', marginTop:5}}>
               Kewanitaan
              </Text>
              </TouchableOpacity>
              </View>
            </Col>
            <Col sm={6} md={4} lg={3}>
              <View style={styles.xxx}>
               <TouchableOpacity activeOpacity={.9}  onPress={() => this.props.navigator.push(routes.kids)}>
              <Avatar icon="child-care" size={105} color={COLOR.indigoA700}/>
              <Text style={{alignSelf:'center', marginTop:5}}>
               Anak & Bayi
              </Text>
              </TouchableOpacity>
              </View>
            </Col>
          </Row>
          <Row size={12}>
            <Col sm={6} md={4} lg={3}>
              <View style={styles.xxx}>
               <TouchableOpacity activeOpacity={.9}  onPress={() => this.props.navigator.push(routes.reminder)}>
                  <Avatar icon="access-alarm" size={105} color={COLOR.blueGrey800}/>
                  <Text style={{alignSelf:'center', marginTop:5}}>
                   Reminder
                  </Text>
                </TouchableOpacity>
              </View>
            </Col>
            <Col sm={6} md={4} lg={3}>
              <View style={styles.xxx}>
               <TouchableOpacity activeOpacity={.9}  onPress={() => this.props.navigator.push(routes.setting)}>
              <Avatar icon="settings" size={105} color={COLOR.yellow700}/>
              <Text style={{alignSelf:'center', marginTop:5}}>
               Settings
              </Text>
               </TouchableOpacity>
              </View>
            </Col>

          </Row>

          </View>

       );

    }
    onAvatarPressed = (value) => {
        const { selected } = this.state;

        const index = selected.indexOf(value);

        if (index >= 0) {
            // remove item
            selected.splice(index, 1);
        } else {
            // add item
            selected.push(value);
        }

        this.setState({ selected });
    }
    renderToolbar = () => {
        if (this.state.selected.length > 0) {
            return (
                <Toolbar
                    key="toolbar"
                    leftElement="info"
                    onLeftElementPress={() => this.setState({ selected: [] })}
                    centerElement='Info'
                    style={{
                        container: { backgroundColor: 'white' },
                        titleText: { color: 'rgba(0,0,0,.87)' },
                        leftElement: { color: 'rgba(0,0,0,.54)' },
                        rightElement: { color: 'rgba(0,0,0,.54)' },
                    }}
                />
            );
        }
        return (
            <Toolbar
                key="toolbar"
                leftElement="menu"
                centerElement={this.props.route.title}
                rightElement={this.renderpicture()}

            />
        );
    }
    renderItem = (title, color, icon, route) => {
        const searchText = this.state.searchText.toLowerCase();

        if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
            return null;
        }

        return (

          <ListItem
              divider
              leftElement={<Avatar icon={icon} size={50} color={color}/>}
              onLeftElementPress={() => this.onAvatarPressed(title)}
              centerElement={title}
              rightElement="info"
              onPress={() => this.props.navigator.push(route)}
          />

        );
    }

    render() {
      let shareOptions = {
        title: "Medclabs Android app",
        message: "Download Medclabs android app",
        url: "https://play.google.com/store/apps/details?id=com.medclabsapp",
        subject: "Medclabs android app" //  for email
      };
        return (

            <Container>

                {this.renderToolbar()}
                <ScrollView
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="interactive"
                    refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh.bind(this)}
                      />
                    }
                >
                <ImageSlider images={[
                     welcomeImg2,
                     welcomeImg2,
                     welcomeImg2
                 ]}/>
                  {this.RenderOffline()}

                </ScrollView>
                <ActionButton buttonColor={COLOR.pink500} bgColor="rgba(0,0,0,0.8)">
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Phone Medclabs" onPress={() => Communications.phonecall('+6281295445045', true)}>
                      <Iconz name="phone" style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                    <ActionButton.Item  buttonColor={COLOR.medcColor} style={styles.icon} title="Share App" onPress={()=> Share.open(shareOptions)}>
                      <Iconx name="share" style={styles.actionButtonIcon} />
                    </ActionButton.Item>

                </ActionButton>
            </Container>


        );
    }
}

Main.propTypes = propTypes;
