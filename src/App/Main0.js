import React, { Component, PropTypes } from 'react';
import { ToastAndroid, ScrollView, Platform, Text, View, StyleSheet, Image } from 'react-native';
import FA from 'react-native-vector-icons/FontAwesome';
import routes from '../routes';
//import Color from 'color';
import Container from '../Container';
import Media from '../Media';
import ControlPanel from '../Drawer/ControlPanel'
//import DrawerSpec from '../Drawer';
import Drawer from 'react-native-drawer'
// components
import { ActionButton, Avatar, Card, ListItem, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
//import { COLOR, ThemeProvider } from '../react-native-material-ui';
import ImageSlider from 'react-native-image-slider';

const welcomeImg = require('./../img/welcome.jpg');
const welcomeImg2 = require('./../img/welcome2.jpg');
const welcomeImg3 = require('./../img/welcome3.jpg');

const styles = StyleSheet.create({
    textContainer: {
        paddingHorizontal: 16,
        paddingBottom: 16,
        paddingTop: 16,
        paddingBottom: 16
    },
    fullImage: {
        alignSelf: 'stretch'
    },
    avatarContainer: {
        paddingHorizontal:20,
        paddingVertical:20,
        flex: 1,
        justifyContent: 'center',

    },
    avatarColor:{
      color: '#000',
    },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    onPress: PropTypes.func,
};

export default class Main extends Component {

  static contextTypes = {
    drawer: PropTypes.object.isRequired,
  };

    constructor(props) {
        super(props);

        this.state = {
            selected: [],
            searchText: '',
            drawerOpen: false,
            drawerDisabled: false,
        };
    }
    closeDrawer = () => {
      this._drawer.close()
    };
    openDrawer = () => {
      this._drawer.open()
    };
    onAvatarPressed = (value) => {
        const { selected } = this.state;

        const index = selected.indexOf(value);

        if (index >= 0) {
            // remove item
            selected.splice(index, 1);
        } else {
            // add item
            selected.push(value);
        }

        this.setState({ selected });
    }
    renderToolbar = () => {
        if (this.state.selected.length > 0) {
            return (
                <Toolbar
                    key="toolbar"
                    leftElement="info"
                    onLeftElementPress={() => this.setState({ selected: [] })}
                    centerElement='Info'
                    style={{
                        container: { backgroundColor: 'white' },
                        titleText: { color: 'rgba(0,0,0,.87)' },
                        leftElement: { color: 'rgba(0,0,0,.54)' },
                        rightElement: { color: 'rgba(0,0,0,.54)' },
                    }}
                />
            );
        }
        return (
            <Toolbar
                key="toolbar"
                leftElement="menu"
                onLeftElementPress=''
                centerElement={this.props.route.title}
                searchable={{
                    autoFocus: true,
                    placeholder: 'Search',
                    onChangeText: value => this.setState({ searchText: value }),
                    onSearchClosed: () => this.setState({ searchText: '' }),
                }}
            />
        );
    }
    renderItem = (title, color, icon, route) => {
        const searchText = this.state.searchText.toLowerCase();

        if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
            return null;
        }

        return (

          <ListItem
              divider
              leftElement={<Avatar icon={icon} size={50} color={color}/>}
              onLeftElementPress={() => this.onAvatarPressed(title)}
              centerElement={title}
              rightElement="info"
              onPress={() => this.props.navigator.push(route)}
          />

        );
    }
    renderDrawer(){
      return(
        <Drawer
          ref={(ref) => this._drawer = ref}
          type="static"
          content={
            <ControlPanel closeDrawer={this.closeDrawer} />
          }
          acceptDoubleTap
          styles={{main: {shadowColor: '#000000', shadowOpacity: 0.3, shadowRadius: 15}}}
          onOpen={() => {
            console.log('onopen')
            this.setState({drawerOpen: true})
          }}
          onClose={() => {
            console.log('onclose')
            this.setState({drawerOpen: false})
          }}
          captureGestures={false}
          tweenDuration={100}
          panThreshold={0.08}
          disabled={this.state.drawerDisabled}
          openDrawerOffset={(viewport) => {
            return 100
          }}
          closedDrawerOffset={() => 50}
          panOpenMask={0.2}
          negotiatePan
          >
        </Drawer>
      );
    }

    render() {
        return (

            <Container>

                {this.renderToolbar()}
                <ScrollView
                    keyboardShouldPersistTaps
                    keyboardDismissMode="always"
                >
                <ImageSlider images={[
                     welcomeImg,
                     welcomeImg2,
                     welcomeImg3
                 ]}/>
                 <View style={{marginTop:10}}>

                 {this.renderItem('Aktivitas', COLOR.tealA700, 'fitness-center', routes.list)}
                 {this.renderItem('Labs', COLOR.teal700,'local-hospital', routes.bottomNavigation)}
                 {this.renderItem('Kids', COLOR.indigoA700,'child-care', routes.dialog)}
                 {this.renderItem('Women', COLOR.pink500,'pregnant-woman', routes.badge)}
                 {this.renderItem('Reminder', COLOR.blueGrey800,'access-alarm', routes.button)}

                 </View>

                </ScrollView>
                <ActionButton
                    actions={[
                        { icon: 'email', label: 'Email' },
                        { icon: 'phone', label: 'Phone' },
                        { icon: 'sms', label: 'Text' },
                        { icon: 'favorite', label: 'Favorite' },
                    ]}
                    icon="help-outline"
                    transition="speedDial"
                    onPress={(action) => {
                        if (Platform.OS === 'android') {
                            ToastAndroid.show(action, ToastAndroid.SHORT);
                        }
                    }}
                />
            </Container>


        );
    }
}

Main.propTypes = propTypes;
