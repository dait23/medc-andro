import React, { Component } from 'react';
import { Navigator, NativeModules, StatusBar, View, AppRegistry, AsyncStorage } from 'react-native';

import { COLOR, ThemeProvider } from '../react-native-material-ui';
import routes from '../routes';
import Container from '../Container';

const UIManager = NativeModules.UIManager;

const uiTheme = {
    palette: {
        primaryColor: COLOR.medcColor,
        accentColor: COLOR.pink500,
    },
};

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      initialComponent:''
    }
  }

    static configureScene(route) {
        return route.animationType || Navigator.SceneConfigs.PushFromRight;
    }
    static renderScene(route, navigator) {
        return (
            <Container>
                <StatusBar backgroundColor="rgba(0, 0, 0, 0.2)" translucent />
                <View style={{ backgroundColor: COLOR.medcColor, height: 24 }} />
                <route.Page
                    route={route}
                    navigator={navigator}
                />
            </Container>
        );
    }
    componentWillMount() {
        if (UIManager.setLayoutAnimationEnabledExperimental) {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }

    }
    render() {
        return (
            <ThemeProvider uiTheme={uiTheme}>
                <Navigator
                    configureScene={App.configureScene}
                    initialRoute={routes.login}
                    ref={this.onNavigatorRef}
                    renderScene={App.renderScene}
                />
            </ThemeProvider>
        );
    }
}

export default App;
