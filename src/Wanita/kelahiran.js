import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  ToastAndroid,
  Platform ,
  TextInput,
  TouchableOpacity,
  DatePickerAndroid
} from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import { ActionButton, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import Icon from 'react-native-vector-icons/FontAwesome';
import Iconx from 'react-native-vector-icons/MaterialIcons';
import Moment from 'moment';
Moment.locale('id') //For Turkey
const myIcon = (<Icon name="rocket" size={30} color="#900" />)
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:COLOR.indigo500
    },
    today: {
      borderWidth:2,
      height:40,
      width:150,
      borderRadius:20,
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf:'center',
      borderColor:'#fff',
      marginTop:30,
      marginBottom:30
    },
    todayText:{
      color:'#fff',
      alignSelf:'center'
    },
    icon:{
      alignSelf:'center',
      color:'#fff'
    },
    boxForm:{
      padding:20,

    },
    input:{
      height:40,
      width:200,
      borderRadius:5,
      backgroundColor:COLOR.grey100,
      marginBottom:10,
      color:'#000',
      paddingLeft:10,
      textAlign:'center',
      alignSelf:'center'
    },
    box:{
      flex:1,
      flexDirection:'row',
      justifyContent: 'space-around',
    },
    buttonContainer:{
      backgroundColor:COLOR.lightBlueA200,
      paddingVertical:15,
      marginBottom:20,
      marginTop:30,
      width:100,
      height:100,
      borderRadius:100,
      alignSelf:'center',
      justifyContent:'center'

    },
    buttonTitle:{
      textAlign:'center',
      color:'#fff',
      fontWeight:'700',

    },
    result:{
      fontSize: 20,
      color: '#fff',
      fontWeight:'700',
      alignSelf:'center'
    },
    result2:{
      fontSize: 11,
      color: '#fff',
      fontWeight:'500',
      alignSelf:'center'
    },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};
const dateTime= new Date();
const formattedDT = Moment(dateTime).format('LL') //2 May

class MasaSubur extends Component {
  constructor(props) {
      super(props);

      this.state = {
          selected: [],
          searchText: '',
          simpleText: 'Menstruasi terakhir Anda',
          showResult: false,
          born: '',
          message: null,
          tglLahir:'',
          bulanLahir:'',
          tahunLahir:'',
          lahir:''


      };
  }
  calculateBorn() {
    date = this.state.simpleText
    d = new Date(date);
    dd = d.getDate();
    mm = d.getMonth()+1; //January is 0!
    yyyy = d.getFullYear();

    tglLahir = dd + 7;
    bulanLahir = mm + 9;
    tahunLahir = yyyy;

    if(tglLahir > 31){
       tglLahir= tglLahir - 31
       }

    if(bulanLahir > 12){
       bulanLahir= bulanLahir - 12
       tahunLahir = tahunLahir + 1
       }
    born = bulanLahir+'/'+tglLahir+'/'+tahunLahir;
    var Lahir = Moment(born).format('LL'); //2 May

    if (born && date !== '') {
      this.setState({
        showResult: true,
        lahir: Lahir,
        message: null
      })
    }

  }
  renderResult(){

    if (this.state.showResult == true){

        return(
          <View>
            <Text style={styles.result}>
               {this.state.lahir}
            </Text>
              <Iconx name="child-care" size={100} style={styles.icon}/>
              <Text style={styles.result2}>
                 Prakiraan tanggal bayi anda lahir
              </Text>
          </View>

        );
    }else{
      return(
        <View>
            <Text style={styles.result2}>
               Tanggal harus di isi 
            </Text>
        </View>
      );
    }


  }
  showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'Text'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'Text'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}

          />
      );
  }
  renderItem = (title, color, icon, route) => {

      return (

        <ListItem
            divider
            leftElement={<Icon name={icon} size={26} color={color} />}
            centerElement={title}
            rightElement="chevron-right"
            onPress={() => this.props.navigator.push(route)}
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>
                <View style={styles.today}>
                  <Text style={styles.todayText}>
                     {formattedDT}
                  </Text>
                </View>
                <View style={styles.boxForm}>
                  <View style={styles.box}>
                      <TextInput
                        style={styles.input}
                        underlineColorAndroid='transparent'
                        placeholder="menstruasi terakhir Anda"
                        placeholderTextColor="rgba(0,0,0,0.7)"
                        keyboardType="default"
                        autoCapitalize="none"
                        autoCorrect={false}
                        onChangeText={(simpleText) => this.setState({simpleText})}
                        returnKeyType="next"
                        value={this.state.simpleText}
                      />
                      <TouchableOpacity
                        onPress={this.showPicker.bind(this, 'simple', {date: this.state.simpleDate})}>
                        <View style={styles.pick}>
                          <Icon name="calendar" size={40} color="#fff" style={{alignSelf:'center', }}/>
                        </View>
                      </TouchableOpacity>
                    </View>

                    <TouchableOpacity activeOpacity={.9} style={styles.buttonContainer} onPress={this.calculateBorn.bind(this)}>
                      <Text style={styles.buttonTitle}>
                        HITUNG
                      </Text>
                    </TouchableOpacity>

                      {this.renderResult()}

                </View>

                </ScrollView>
            </View>
        );
    }
}

MasaSubur.propTypes = propTypes;

export default MasaSubur;
