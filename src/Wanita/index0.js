import { Text, Dimensions, View, StyleSheet, ScrollView, ToastAndroid, Platform, Image } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import { IconToggle, Card, Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImageSlider from 'react-native-image-slider';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const welcomeImg = require('./../img/welcome.jpg');

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
    actionButtonIcon: {
    fontSize: 30,
    height: 32,
    color: 'white',
  },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};
class Wanita extends Component {
  constructor(props) {
      super(props);

      this.state = {
          selected: [],
          searchText: '',

      };
  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              searchable={{
                  autoFocus: true,
                  placeholder: 'Search',
                  onChangeText: value => this.setState({ searchText: value }),
                  onSearchClosed: () => this.setState({ searchText: '' }),
              }}
          />
      );
  }
  renderItem = (title, color, icon) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={40} color={color}/>}
            centerElement={title}
            rightElement="delete"
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="Aktivitas Terbaru"/>


                    <View style={{marginTop:0, backgroundColor:'#f9f9f9'}}>

                        <Row size={12} style={styles.Rm} nowarp>
                          <Col sm={4} md={4} lg={3}>
                          <View style={styles.box}>
                            <Text style={{fontSize:14}}>Masa Subur</Text>
                             <IconToggle name="pregnant-woman" color={COLOR.pink500}  size={30}/>
                             <Text style={{fontSize:26}}>10 Hari</Text>
                          </View>
                          </Col>
                          <Col sm={4} md={4} lg={3}>
                          <View style={styles.box}>
                          <Text style={{fontSize:14}}>Kelahiran</Text>
                           <IconToggle name="child-care" color={COLOR.pink500}  size={30}/>
                           <Text style={{fontSize:26}}>12</Text>
                           <Text style={{fontSize:12}}>Minggu</Text>
                          </View>
                          </Col>
                          <Col sm={4} md={4} lg={3}>
                          <View style={styles.box}>
                          <Text style={{fontSize:14}}>PMS</Text>
                           <IconToggle name="pregnant-woman" color={COLOR.pink500}  size={30}/>
                           <Text style={{fontSize:26}}>1</Text>
                           <Text style={{fontSize:12}}>Bulan</Text>
                          </View>
                          </Col>

                        </Row>

                    <Subheader text="My List"/>
                    {this.renderItem('Masa Subur', COLOR.pink500, 'pregnant-woman')}
                    {this.renderItem('Kelahiran', COLOR.pink500,'child-care')}
                    {this.renderItem('PMS', COLOR.pink500,'pregnant-woman')}
                    {this.renderItem('Masa Subur', COLOR.pink500, 'pregnant-woman')}
                    {this.renderItem('Kelahiran', COLOR.pink500,'child-care')}
                    {this.renderItem('PMS', COLOR.pink500,'pregnant-woman')}

                    </View>

                </ScrollView>
                <ActionButton buttonColor={COLOR.pink500} bgColor="rgba(0,0,0,0.8)">
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Masa Subur"  onPress={() => this.props.navigator.push(routes.masasubur)}>
                      <Icon name="child-friendly" style={styles.actionButtonIcon} />
                    </ActionButton.Item>
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Waktu Kelahiran"  onPress={() => this.props.navigator.push(routes.kelahiran)}>
                      <Icon name="pregnant-woman" style={styles.actionButtonIcon} />
                    </ActionButton.Item>

                </ActionButton>

            </View>
        );
    }
}

Wanita.propTypes = propTypes;

export default Wanita;
