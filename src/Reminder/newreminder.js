import { Text, View, TextInput, TimePickerAndroid, DatePickerAndroid, Picker, StyleSheet, TouchableOpacity, ScrollView, ToastAndroid, Platform } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconx from 'react-native-vector-icons/FontAwesome';
import { Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  formContainer:{
    padding:15,
  },
  input:{
    height:40,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10,
  },
  inputPick:{
    height:40,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10,
    width:225
  },
  area:{
    height:70,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10
  },
  buttonContainer:{
    backgroundColor:COLOR.medcColor,
    paddingVertical:15,
    marginBottom:20,

  },
  buttonTitle:{
    textAlign:'center',
    color:'#fff',
    fontWeight:'700',

  },
  box:{
    flex:1,
    flexDirection:'row',
    justifyContent: 'space-around',
  },
  pick:{
    height:40,
    marginBottom:10,
    color:'#fff',
    textAlign:'center',
    width:100,
    justifyContent: 'center',
    paddingVertical:10,
  },
});
const Item = Picker.Item;
const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

function _formatTime(hour, minute) {
  return hour + ':' + (minute < 10 ? '0' + minute : minute);
}

class NewReminder extends Component {

  constructor(props) {
    super(props);
    this.onReminder = this.onReminder.bind(this);
    this.state = {
      selectedItem: undefined,
      title:'',
      kategori: '',
      keterangan:'',
      simpleText: 'pilih Tanggal',
      presetText: 'Pilih Waktu',
      reminder_day:'',
      reminder_time:'',
      data:{},
    };
  }

  focusChangeField = (focusField) => {
    this.refs[focusField].focus();
  }

  onValueChange(value: string) {
    this.setState({
      kategori: value,
    });
  }

  renderToolbar = () => {
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
          />
      );
  }
  showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'Text'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'Text'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };
  showPickerTime = async (stateKey, options) => {
    try {
      const {action, minute, hour} = await TimePickerAndroid.open(options);
      var newState = {};
      if (action === TimePickerAndroid.timeSetAction) {
        newState[stateKey + 'Text'] = _formatTime(hour, minute);
        newState[stateKey + 'Hour'] = hour;
        newState[stateKey + 'Minute'] = minute;
      } else if (action === TimePickerAndroid.dismissedAction) {
        newState[stateKey + 'Text'] = 'dismissed';
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };
  onReminder= () =>  {

      var url =  DevApi + 'reminder/add_reminder';
      var that = this;
      return fetch(url, {
              method: 'POST',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              },
              body: JSON.stringify({

                'title' : this.state.title,
                'kategori' : this.state.kategori,
                'keterangan' : this.state.keterangan,
                'reminder_day': this.state.simpleText,
                'reminder_time': this.state.presetText,


              }),
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          if (result.data.status == '200') {
            ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
            that.props.navigator.pop();
            that.setState({ data : result.data });
          }
          else{
          ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
        }
        })
        .catch((error) => { console.error(error); });
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="Add New"/>

                    <View style={styles.formContainer}>
                        <TextInput
                          style={styles.input}
                          underlineColorAndroid='transparent'
                          placeholder="Title"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(title) => this.setState({title})}
                          returnKeyType="next"
                          value={this.state.title}
                        />
                        <Picker
                          mode="dialog"
                          selectedValue={this.state.kategori}
                          onValueChange={this.onValueChange.bind(this)}
                          style={styles.input}
                        >
                          <Item label="Aktivitas" value="aktivitas" />
                          <Item label="kids" value="kids" />
                          <Item label="Kebidanan" value="kebidanan" />
                          <Item label="Others" value="others" />
                        </Picker>
                        <View style={styles.box}>
                        <TextInput
                          style={styles.inputPick}
                          underlineColorAndroid='transparent'
                          placeholder="Day"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(simpleText) => this.setState({simpleText})}
                          returnKeyType="next"
                          value={this.state.simpleText}
                        />
                          <TouchableOpacity
                            onPress={this.showPicker.bind(this, 'simple', {date: this.state.simpleDate})}>
                            <View style={styles.pick}>
                              <Iconx name="calendar" size={28} color={COLOR.medcColor} style={{alignSelf:'center'}}/>
                            </View>
                          </TouchableOpacity>
                        </View>
                        <View style={styles.box}>
                        <TextInput
                          style={styles.inputPick}
                          underlineColorAndroid='transparent'
                          placeholder="Pilih Waktu"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(presetText) => this.setState({presetText})}
                          returnKeyType="next"
                          value={this.state.presetText}
                        />
                          <TouchableOpacity
                          onPress={this.showPickerTime.bind(this, 'preset', {
                            hour: this.state.presetHour,
                            minute: this.state.presetMinute,
                          })}>
                            <View style={styles.pick}>
                              <Iconx name="clock-o" size={28} color={COLOR.medcColor} style={{alignSelf:'center'}}/>
                            </View>
                          </TouchableOpacity>
                        </View>
                        <TextInput
                          style={styles.area}
                          underlineColorAndroid='transparent'
                          placeholder="Keterangan"
                          placeholderTextColor="rgba(0,0,0,0.8)"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          onChangeText={(keterangan) => this.setState({keterangan})}
                          returnKeyType="done"
                          value={this.state.keterangan}
                        />
                        <TouchableOpacity activeOpacity={.9} style={styles.buttonContainer} onPress={this.onReminder}>
                          <Text style={styles.buttonTitle}>
                            SAVE REMINDER
                          </Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>


            </View>
        );
    }
}

NewReminder.propTypes = propTypes;

export default NewReminder;
