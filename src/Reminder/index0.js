import { Text, View, StyleSheet, ScrollView, ToastAndroid, Platform, ActivityIndicator } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    actionButtonIcon: {
      fontSize: 30,
      height: 32,
      color: 'white',
  },
  centering: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
  },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

class Reminder extends Component {
  constructor(props) {
      super(props);
      this.getReminder = this.getReminder.bind(this)
      this.state = {
          selected: [],
          searchText: '',
          data: [],
          loading: true,
          animating: true,

      };
  }
  showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'Text'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'Text'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };
  componentDidMount() {

      var that = this;
      this.getReminder();
      that.setToggleTimeout();
  }
  componentWillUnmount() {
    clearTimeout(this._timer);
  }

  setToggleTimeout() {
    this._timer = setTimeout(() => {
      this.setState({animating: !this.state.animating});
      this.setToggleTimeout();
    }, 2000);
  }
  getReminder() {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });

      var url =  DevApi + 'reminder';
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          //componentWillUnmount();
          that.setState({ data : result,
                          loading: false

                        });
           //data : this.state.data(result.data),

        })
        .catch((error) => { console.error(error); });
  }

  renderLoading = () =>{

    if (this.state.loading) {
      return (
        <ActivityIndicator
        animating={this.state.animating}
        style={[styles.centering, {height: 50}]}
        size="30"
       />
        );

    }else{

        if(this.state.data.status == '200'){

          return(
             <View>
             {this.state.data.data.map((item, i)=>{

                return (

                 <View>
                    {this.renderItem(item.title, COLOR.tealA700, 'alarm-add')}
                 </View>

                );

              })}
             </View>
          );
        }

    }


  }


  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              searchable={{
                  autoFocus: true,
                  placeholder: 'Search',
                  onChangeText: value => this.setState({ searchText: value }),
                  onSearchClosed: () => this.setState({ searchText: '' }),
              }}
          />
      );
  }
  renderItem = (title, color, icon) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={30} color={color}/>}
            centerElement={title}
            rightElement="delete"
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="Reminder List"/>

                    <View style={{marginTop:10}}>

                    {this.renderItem('My items Reminder list 1', COLOR.tealA700, 'fitness-center')}
                    {this.renderItem('My items Reminder list 2', COLOR.teal700,'local-hospital')}
                    {this.renderItem('My items Reminder list 3', COLOR.indigoA700,'child-care')}
                    {this.renderItem('My items Reminder list 4', COLOR.pink500,'pregnant-woman')}
                    {this.renderItem('My items Reminder list 1', COLOR.tealA700, 'fitness-center')}
                    {this.renderItem('My items Reminder list 2', COLOR.teal700,'local-hospital')}
                    {this.renderItem('My items Reminder list 3', COLOR.indigoA700,'child-care')}
                    {this.renderItem('My items Reminder list 4', COLOR.pink500,'pregnant-woman')}

                    </View>

                </ScrollView>
                <ActionButton buttonColor={COLOR.pink500} bgColor="rgba(0,0,0,0.8)">
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="New Reminder"  onPress={() => this.props.navigator.push(routes.newreminder)}>
                      <Icon name="alarm-add" style={styles.actionButtonIcon} />
                    </ActionButton.Item>

                </ActionButton>

            </View>
        );
    }
}

Reminder.propTypes = propTypes;

export default Reminder;
