import React, { Component, PropTypes } from 'react';
import {
ActivityIndicator,
StyleSheet,
View,
AsyncStorage,
Image
} from 'react-native';

import routes from '../routes';
//import Color from 'color';
import Container from '../Container';

// components
import {  COLOR, ThemeProvider} from '../react-native-material-ui/src';

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    onPress: PropTypes.func
};

const background = require("../img/launchscreen-bg.png");

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  centering: {
      flexDirection:'row',
      alignItems: 'center',
      justifyContent: 'center',
      padding: 8,
    },
  background: {
    flex: 1,
    width: null,
    height: null,

  },

});

type State = { animating: boolean; };
type Timer = number;

class Splash extends Component {

  state: State;
  _timer: Timer;

  constructor(props) {
    super(props);
    this.state = {
      animating: true,
    };
  }


  componentDidMount(){

      this.setToggleTimeout();

  }

	componentWillMount() {
     AsyncStorage.getItem("@userLoggedIn").then((status)=> {
      console.log(status);
      if(status=="true") {
        this.props.navigator.push(routes.main);
      //  var Mainx = this.state.initialComponent;
        //this.refs['MainNavigator'].push({name:this.state.initialComponent});
        //console.log(Mainx);
      }else{
        this.props.navigator.push(routes.login);
      }
    }).done();
 }
 componentWillUnmount() {
   clearTimeout(this._timer);
 }

 setToggleTimeout() {
   this._timer = setTimeout(() => {
     this.setState({animating: !this.state.animating});
     this.setToggleTimeout();
   }, 1000);
 }
  render() {
    return (
      <View style={styles.container}>
        <Image source={background} style={styles.background}>

        <ActivityIndicator
           animating={this.state.animating}
           style={[styles.centering, {height: 200}]}
           size={75}
           color="white"
         />


        </Image>
      </View>
    );
  }

}


Splash.propTypes = propTypes;

export default Splash;
