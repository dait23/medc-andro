import React, { Component, PropTypes } from 'react';
import {
ScrollView,
StyleSheet,
Text,
View,
Image,
Dimensions,
TextInput,
Button,
Alert,
TouchableOpacity,
AsyncStorage,
ToastAndroid,
KeyboardAvoidingView
} from 'react-native';

import routes from '../routes';
//import Color from 'color';
import Container from '../Container';
import {DevApi, MainApi} from '../Api';
// components
import {  COLOR, ThemeProvider} from '../react-native-material-ui/src';

const { width, height } = Dimensions.get("window");

const background = require("../img/launchscreen-bg.png");
const mark = require("./medclogo.png");
const lockIcon = require("./login1_lock.png");
const personIcon = require("./login1_person.png");
const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  markWrap: {
    flex: 1,
    paddingVertical: 30,
  },
  mark: {
    width: null,
    height: null,
    flex: 1,
  },
  background: {
    flex: 1,
    width: null,
    height: null,
  },
  wrapper: {
    paddingVertical: 30,
  },
  inputWrap: {
    flexDirection: "row",
    marginVertical: 10,
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: "#CCC"
  },
  iconWrap: {
    paddingHorizontal: 7,
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    height: 20,
    width: 20,
  },
  input: {
    flex: 1,
    paddingHorizontal: 10,
    color:'#fff'
  },
  button: {
    backgroundColor: "#c51162",
    paddingVertical: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
  },
  buttonText: {
    color: "#FFF",
    fontSize: 18,
  },
  forgotPasswordText: {
    color: "#D8D8D8",
    backgroundColor: "transparent",
    textAlign: "right",
    paddingRight: 15,
  },
  signupWrap: {
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  accountText: {
    color: "#D8D8D8"
  },
  signupLinkText: {
    color: "#FFF",
    marginLeft: 5,
  },
  logoContainer: {
    flex: 1,
    marginTop: deviceHeight / 10,
    marginBottom: 10,
    alignSelf: 'center',
  },
  logo: {

    width: 80,
    height: 80,
    alignSelf: 'center'
  },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    onPress: PropTypes.func
};

class Login extends Component {

  constructor(props) {
    super(props);
    this.onLoginPress = this.onLoginPress.bind(this);
    this.state = {
      noreg: "",
      password: "",
      error: "",
      logIn: false,
      data : {}
    };
  }
  focusChangeField = (focusField) => {
    this.refs[focusField].focus();
  }

  componentWillMount() {
      AsyncStorage.getItem('@userDetails').then((value)=> {
        console.log(JSON.parse(value));
      }).done();
  }
  focusChangeField = (focusField) => {
    this.refs[focusField].focus();
  }

  onLoginPress() {

    let noreg = this.state.noreg;
    let password = this.state.password;
    let userDetails = {
      noreg:noreg,
      password:password
    }

      var url =  DevApi + 'auth/login';
      var that = this;
      return fetch(url, {
              method: 'POST',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              },
              body: JSON.stringify({

                'noreg' : this.state.noreg,
                'password': this.state.password


              }),
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
            if (result.data.status == '200') {
              //AsyncStorage.setItem('@userDetails', JSON.stringify(userDetails));
              //AsyncStorage.setItem("@userLoggedIn","true");
              ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
              that.props.navigator.push(routes.main);
            }else{
            ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
            }


          that.setState({ data : result.data, loginder: false  });
        })
        .catch((error) => { console.error(error); });
    }

  render() {
    return (
      <View style={styles.container}>
        <Image source={background} style={styles.background}>
          <View style={styles.logoContainer}>
            <Image source={mark} style={styles.logo} />
          </View>
          <View style={styles.wrapper}>
            <View style={styles.inputWrap}>
              <View style={styles.iconWrap}>
                <Image source={personIcon} style={styles.icon} resizeMode="contain" />
              </View>
              <TextInput
                placeholder="Nomer Registrasi / Email Anda"
                placeholderTextColor="#FFF"
                style={styles.input}
                underlineColorAndroid='transparent'
                keyboardType="default"
                returnKeyType="next"
                ref="loginNoreg"
                onChangeText={(noreg) => this.setState({noreg})}
                onSubmitEditing={() => this.focusChangeField('loginPassword')}
                value={this.state.noreg}
              />
            </View>
            <View style={styles.inputWrap}>
              <View style={styles.iconWrap}>
                <Image source={lockIcon} style={styles.icon} resizeMode="contain" />
              </View>
              <TextInput
                placeholderTextColor="#FFF"
                placeholder="Password Anda"
                style={styles.input}
                secureTextEntry
                underlineColorAndroid='transparent'
                onChangeText={(password) => this.setState({password})}
                keyboardType="default"
                ref="loginPassword"
                returnKeyType="done"
                onSubmitEditing={this.onLoginPress}
                value={this.state.password}
              />
            </View>

            <TouchableOpacity activeOpacity={.5} onPress={this.onLoginPress}>
              <View style={styles.button}>
                <Text style={styles.buttonText}>Sign In</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.container}>
            <View style={styles.signupWrap}>
              <Text style={styles.accountText}>Belum punya Nomer Register?</Text>
              <TouchableOpacity activeOpacity={.5}   onPress={() => this.props.navigator.push(routes.register)}>
                <View>
                  <Text style={styles.signupLinkText}>Sign Up</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Image>
      </View>
    );
  }

}


Login.propTypes = propTypes;

export default Login;
