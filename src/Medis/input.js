import {
  Text,
  View,
  TextInput,
  DatePickerAndroid,
  Picker,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ToastAndroid,
  Platform,
  Image,
  ActivityIndicator } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Iconx from 'react-native-vector-icons/FontAwesome';
import { Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImagePicker from 'react-native-image-picker'
import RNFetchBlob from 'react-native-fetch-blob'
import FbApp from '../Firebase/firebase'
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  formContainer:{
    padding:15,
  },
  input:{
    height:40,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10,
  },
  inputPick:{
    height:40,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10,
    width:225
  },
  area:{
    height:70,
    backgroundColor:COLOR.grey100,
    marginBottom:10,
    color:'#000',
    paddingLeft:10
  },
  buttonContainer:{
    backgroundColor:COLOR.medcColor,
    paddingVertical:15,
    marginBottom:20,

  },
  buttonTitle:{
    textAlign:'center',
    color:'#fff',
    fontWeight:'700',

  },
  box:{
    flex:1,
    flexDirection:'row',
    justifyContent: 'space-around',
  },
  pick:{
    height:40,
    marginBottom:10,
    textAlign:'center',
    width:100,
    justifyContent: 'center',
    paddingVertical:10,
  },
  camera:{
      alignItems:'center',
      marginBottom:30,
      marginTop:20
  },
  image: {
    height: 100,
    width:100,
    borderRadius:100,
    alignSelf:'center',

  },
  upload: {
    textAlign: 'center',
    color: '#333333',
    padding: 10,
    marginBottom: 5,
    borderWidth: 1,
    borderColor: 'gray'
  },
});
const Item = Picker.Item;
const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};

const storage = FbApp.storage();

// Prepare Blob support
const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

const uploadImage = (uri, mime = 'application/octet-stream') => {
  return new Promise((resolve, reject) => {
    const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri
    const sessionId = new Date().getTime()
    let uploadBlob = null
    const imageRef = storage.ref('images').child(`${sessionId}`)

    fs.readFile(uploadUri, 'base64')
      .then((data) => {
        return Blob.build(data, { type: `${mime};BASE64` })
      })
      .then((blob) => {
        uploadBlob = blob
        return imageRef.put(blob, { contentType: mime })
      })
      .then(() => {
        uploadBlob.close()
        return imageRef.getDownloadURL()
      })
      .then((url) => {
        resolve(url)
      })
      .catch((error) => {
        reject(error)
    })
  })
}




class InputMedis extends Component {

  constructor(props) {
    super(props);
    this.onMedisPress = this.onMedisPress.bind(this);
    this.state = {
      selectedItem: undefined,
      selected1: 'aktivitas',
      simpleText: 'Pilih Tanggal',
      title:'',
      tempat:'',
      keterangan:'',
      results: {
        items: [],
      },
      data:{},
    };
  }
  _pickImage() {
    this.setState({ uploadURL: '' })

    ImagePicker.showImagePicker({}, response  => {
      uploadImage(response.uri)
        .then(url => this.setState({ uploadURL: url }))
        .catch(error => console.log(error))
    })
  }
  focusChangeField = (focusField) => {
    this.refs[focusField].focus();
  }

  onValueChange(value: string) {
    this.setState({
      selected1: value,
    });
  }
  renderToolbar = () => {
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
          />
      );
  }
  showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
        newState[stateKey + 'Text'] = 'dismissed';
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + 'Text'] = date.toLocaleDateString();
        newState[stateKey + 'Date'] = date;
      }
      this.setState(newState);
    } catch ({code, message}) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };
  onMedisPress= () =>  {

      var url =  DevApi + 'medis/add_medis';
      var that = this;
      return fetch(url, {
              method: 'POST',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              },
              body: JSON.stringify({

                'title' : this.state.title,
                'tanggal' : this.state.simpleText,
                'keterangan' : this.state.keterangan,
                'tempat': this.state.tempat,
                'link_picture': this.state.uploadURL,


              }),
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          if (result.data.status == '200') {
            ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
            that.props.navigator.pop();
            that.setState({ data : result.data });
          }
          else{
          ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
        }
        })
        .catch((error) => { console.error(error); });
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView style={styles.container}>

                    <Subheader text="Add New"/>


                    {
          (() => {
            switch (this.state.uploadURL) {
              case null:
                return null
              case '':
                return <ActivityIndicator />
              default:
                return (
                  <View>
                    <Image
                      source={{ uri: this.state.uploadURL }}
                      style={ styles.image }
                    />
                  </View>
                )
            }
          })()
        }

        <View style={styles.formContainer}>
          <TouchableOpacity onPress={ () => this._pickImage() }>
            <View style={styles.camera}>
              <Avatar icon="camera-alt" size={100} color={COLOR.teal700}/>
            </View>
        </TouchableOpacity>
            <TextInput
              style={styles.input}
              underlineColorAndroid='transparent'
              placeholder="Title"
              placeholderTextColor="rgba(0,0,0,0.8)"
              keyboardType="default"
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={(title) => this.setState({title})}
              returnKeyType="next"
              value={this.state.title}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid='transparent'
              placeholder="Tempat / Labs / Rumah Sakit"
              placeholderTextColor="rgba(0,0,0,0.8)"
              keyboardType="default"
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={(tempat) => this.setState({tempat})}
              returnKeyType="next"
              value={this.state.tempat}
            />
            <View style={styles.box}>
            <TextInput
              style={styles.inputPick}
              underlineColorAndroid='transparent'
              placeholder="Day"
              placeholderTextColor="rgba(0,0,0,0.8)"
              keyboardType="default"
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={(simpleText) => this.setState({simpleText})}
              returnKeyType="next"
              value={this.state.simpleText}
            />
              <TouchableOpacity
                onPress={this.showPicker.bind(this, 'simple', {date: this.state.simpleDate})}>
                <View style={styles.pick}>
                  <Iconx name="calendar" size={28} color={COLOR.medcColor} style={{alignSelf:'center'}}/>
                </View>
              </TouchableOpacity>
            </View>
            <TextInput
              style={styles.area}
              underlineColorAndroid='transparent'
              placeholder="Keterangan"
              placeholderTextColor="rgba(0,0,0,0.8)"
              keyboardType="default"
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={(keterangan) => this.setState({keterangan})}
              returnKeyType="done"
              value={this.state.keterangan}
            />
            <TouchableOpacity activeOpacity={.5} style={styles.buttonContainer} onPress={this.onMedisPress}>
              <Text style={styles.buttonTitle}>
                ADD
              </Text>
            </TouchableOpacity>
        </View>


                 </ScrollView>


            </View>
        );
    }
}

InputMedis.propTypes = propTypes;

export default InputMedis;
