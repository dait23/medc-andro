import { Text, Dimensions,  NetInfo, RefreshControl, View, StyleSheet, ScrollView, ToastAndroid, Platform, Image } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import NetPage from '../Net';
import { ActionButton, IconToggle, Card, Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImageSlider from 'react-native-image-slider';
import Iconx from 'react-native-vector-icons/FontAwesome';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
import Moment from 'moment';

Moment.locale('id') //For Turkey
const welcomeImg = require('./../img/welcome.jpg');

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
    kidsbox:{
      alignItems:'center',
      alignSelf:'center',
      justifyContent: "center",
      marginBottom:20,
      width:null,
    },
    kidsname:{
      fontSize:16,
      fontWeight:'bold',
      alignSelf:'center',
      marginTop:5,
      marginBottom:5,
    },
    kidsfemale:{
      color:COLOR.pink400,
      alignSelf:'center',
    },
    kidsmale:{
      color:COLOR.medcColor,
      alignSelf:'center',
    },
    boxPicture:{
      flex:1,
      flexDirection:'row',
      justifyContent: 'space-around',
      alignItems: 'center'
    },

});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    title: PropTypes.object.isRequired,
};
class DetailMedis extends Component {
  constructor(props) {
      super(props);
      this.detailMedis = this.detailMedis.bind(this)
      this.state = {
          selected: [],
          searchText: '',
          data : {},
          refreshing: false,
           isConnected: true,
      };
  }
  componentDidMount() {

      var that = this;
      this.detailMedis();
      NetInfo.isConnected.addEventListener(
     'change',
     this._handleConnectivityChange
     );
     NetInfo.isConnected.fetch().done(
         (isConnected) => { this.setState({isConnected}); }
     );

  }
  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'change',
      this._handleConnectivityChange
  );
}
_handleConnectivityChange = (isConnected) => {
this.setState({
  isConnected,
});
};

  _onRefresh() {
    this.setState({refreshing: true});
    this.detailMedis().then(() => {
      this.setState({refreshing: false});
    });
  }

  detailMedis() {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });

      var ID = this.props.route.id
      var url =  DevApi + 'medis/detail/' + ID;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              }
        })
        .then(function(response) {
        return response.json();
      }).then(function(result) {
        that.setState({
          data : result.data,
          id:result.data.id,
          title:result.data.title,
          tanggal: result.data.tanggal,
          tempat:result.data.tempat,
          keterangan:result.data.keterangan,
          link_picture: result.data.link_picture,
         });
      })
      .catch((error) => { console.error(error); });
  }
  delMedis() {
    var url =  DevApi + 'medis/delete/' + this.state.id;
    var that = this;
    return fetch(url, {
            method: 'DELETE',
            headers: {
              'Client-Service': 'mobile-client',
              'Content-Type': 'application/json',
              'Auth-key': 'medcrestapi'
            }
      })
      .then(function(response) {
        return response.json();
      }).then(function(result) {
        ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
        that.props.navigator.pop()
        this.setState({
          data : result,
          loading: true
        });
        //componentWillUpdate();
         //data : this.state.data(result.data),

      })
        ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
      .catch((error) => { console.error(error); });
    }
  RenderOffline(){

    if(this.state.isConnected == false){

        return(
            <View>
             <NetPage />
            </View>

        );
    }else{

       return(

         <View>
         {this.renderMain()}
         </View>
       )
    }
  }
  renderMain(){

    const dateTime= this.state.tanggal;

    const formattedDT = Moment(dateTime).format('LL') //2 May
  //  const lahir = this.state.tempat_lahir + "," + " " + " " + formattedDT


    return(

      <View>
      {this.renderpicture()}

      <Subheader text="Medis"/>
        {this.renderItem(this.state.title, COLOR.medcColor, 'local-pharmacy')}
        {this.renderItem(formattedDT, COLOR.medcColor,'today')}
        {this.renderItem(this.state.tempat, COLOR.medcColor,'local-hospital')}
        {this.renderItem(this.state.keterangan, COLOR.medcColor, 'event-note')}

      </View>

    );


  }
  renderpicture(){

    return(

      <View style={styles.kidsbox}>
         <Image source={{ uri: this.state.link_picture }} style={{  width: deviceWidth , height: deviceHeight / 3.6 , borderWidth:0,borderColor:COLOR.medcColor, alignSelf:'center', resizeMode:"cover"}}/>
      </View>

    );

  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              rightElement="delete"
              onRightElementPress={() => this.delMedis()}
          />
      );
  }
  renderItem = (title, color, icon) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={40} color={color}/>}
            centerElement={title}

        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView
                style={styles.container}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh.bind(this)}
                  />
                }
                >
                  <View style={{marginTop:0, backgroundColor:'#f9f9f9'}}>
                   {this.RenderOffline()}
                  </View>
                </ScrollView>
            </View>
        );
    }
}

DetailMedis.propTypes = propTypes;

export default DetailMedis;
