import { Text, Dimensions,  NetInfo, RefreshControl, View, StyleSheet, ScrollView, ToastAndroid, Platform, Image } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import NetPage from '../Net';
import { ActionButton, IconToggle, Card, Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImageSlider from 'react-native-image-slider';
import Iconx from 'react-native-vector-icons/FontAwesome';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
import Moment from 'moment';
import {Column as Col, Row} from 'react-native-flexbox-grid';
Moment.locale('id') //For Turkey
const welcomeImg = require('./../img/welcome.jpg');

const asam_urat = require('./../img/1.png');
const tekanan_darah = require('./../img/2.png');
const kolesterol = require('./../img/3.png');
const gula_darah = require('./../img/4.png');

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    Rm:{
      marginBottom:3,
    },
    kidsbox:{
      alignItems:'center',
      alignSelf:'center',
      justifyContent: "center",
      marginBottom:20,
      width:null,
    },
    kidsname:{
      fontSize:16,
      fontWeight:'bold',
      alignSelf:'center',
      marginTop:5,
      marginBottom:5,
    },
    kidsfemale:{
      color:COLOR.pink400,
      alignSelf:'center',
    },
    kidsmale:{
      color:COLOR.medcColor,
      alignSelf:'center',
    },
    boxPicture:{
      flex:1,
      flexDirection:'row',
      justifyContent: 'space-around',
      alignItems: 'center'
    },
    kidsbox:{
      alignItems:'center',
      alignSelf:'center',
      justifyContent: "center",
      height: deviceHeight / 3.8,
      marginTop:30,
      marginBottom:20,
    },
    kidsname:{
      fontSize:16,
      fontWeight:'bold',
      alignSelf:'center',
      marginTop:5,
      marginBottom:5,
    },
    kidsnamex:{
      fontSize:12,
      alignSelf:'center',
      marginTop:5,
      marginBottom:5,
    },
    boxPicture:{
      flex:1,
      flexDirection:'row',
      justifyContent: 'space-around',
      alignItems: 'center'
    },
    textContainer: {
        paddingHorizontal: 16,
        paddingBottom: 16,
        paddingTop:16,

    },
    ket:{
      fontSize:16,
    }
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    title: PropTypes.object.isRequired,
};
class DetailRiwayat extends Component {
  constructor(props) {
      super(props);
      this.detailMedis = this.detailMedis.bind(this)
      this.state = {
          selected: [],
          searchText: '',
          data : {},
          refreshing: false,
           isConnected: true,
      };
  }
  componentDidMount() {

      var that = this;
      this.detailMedis();
      NetInfo.isConnected.addEventListener(
     'change',
     this._handleConnectivityChange
     );
     NetInfo.isConnected.fetch().done(
         (isConnected) => { this.setState({isConnected}); }
     );

  }
  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'change',
      this._handleConnectivityChange
  );
}
_handleConnectivityChange = (isConnected) => {
this.setState({
  isConnected,
});
};

  _onRefresh() {
    this.setState({refreshing: true});
    this.detailMedis().then(() => {
      this.setState({refreshing: false});
    });
  }
  delMedis(ID) {
    var url =  DevApi + 'medis/delete/' + ID;
    var that = this;
    return fetch(url, {
            method: 'DELETE',
            headers: {
              'Client-Service': 'mobile-client',
              'Content-Type': 'application/json',
              'Auth-key': 'medcrestapi'
            }
      })
      .then(function(response) {
        return response.json();
      }).then(function(result) {
        ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
        that.props.navigator.pop()
        this.setState({
          data : result,
          loading: true
        });
        //componentWillUpdate();
         //data : this.state.data(result.data),

      })
        ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
      .catch((error) => { console.error(error); });
    }
  detailMedis() {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });

      var ID = this.props.route.id
      var url =  DevApi + 'riwayat/detail/' + ID;
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              }
        })
        .then(function(response) {
        return response.json();
      }).then(function(result) {
        that.setState({
          data : result.data,
          tanggal:result.data.tanggal,
          cek_asamurat: result.data.cek_asamurat,
          cek_guladarah:result.data.cek_guladarah,
          cek_kolesterol:result.data.cek_kolesterol,
          cek_tekanandarah:result.data.cek_tekanandarah,
          keterangan: result.data.keterangan,
         });
      })
      .catch((error) => { console.error(error); });
  }
  RenderOffline(){

    if(this.state.isConnected == false){

        return(
            <View>
             <NetPage />
            </View>

        );
    }else{

       return(

         <View>
         {this.renderMain()}
         </View>
       )
    }
  }
  renderMain(){

    const dateTime= this.state.tanggal;

    const formattedDT = Moment(dateTime).format('LL') //2 May
  //  const lahir = this.state.tempat_lahir + "," + " " + " " + formattedDT


    return(

       <View>
         <Row size={12} style={{marginTop:30}}>
            <Col sm={6} md={4} lg={3}>
            <Image source={tekanan_darah} style={{ width:80, height:80, alignSelf:'center'}}/>
            <Text style={styles.kidsname}>Tekanan Darah</Text>
            <Text style={styles.kidsnamex}>{this.state.cek_tekanandarah}</Text>
            </Col>
            <Col sm={6} md={4} lg={3}>
            <Image source={asam_urat} style={{ width:80, height:80, alignSelf:'center'}}/>
            <Text style={styles.kidsname}>Asam Urat</Text>
            <Text style={styles.kidsnamex}>{this.state.cek_asamurat}</Text>
            </Col>
          </Row>
         <Row size={12} style={{marginTop:30}}>
             <Col sm={6} md={4} lg={3}>
             <Image source={gula_darah} style={{ width:80, height:80, alignSelf:'center'}}/>
             <Text style={styles.kidsname}>Gula Darah</Text>
             <Text style={styles.kidsnamex}>{this.state.cek_guladarah}</Text>
             </Col>
             <Col sm={6} md={4} lg={3}>
             <Image source={kolesterol} style={{ width:80, height:80, alignSelf:'center'}}/>
             <Text style={styles.kidsname}>Kolesterol</Text>
             <Text style={styles.kidsnamex}>{this.state.cek_kolesterol}</Text>
             </Col>
           </Row>
             <Subheader text="Keterangan"/>
           <Card>

               <View style={styles.textContainer}>
                   <Text style={styles.ket}>
                       {this.state.keterangan}
                   </Text>
               </View>
           </Card>
       </View>

    );


  }
  renderpicture(){

    return(

      <View style={styles.kidsbox}>
         <Image source={{ uri: this.state.link_picture }} style={{  width: deviceWidth , height: deviceHeight / 3.6 , borderWidth:0,borderColor:COLOR.medcColor, alignSelf:'center', resizeMode:"cover"}}/>
      </View>

    );

  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}

          />
      );
  }
  renderItem = (title, color, icon) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={40} color={color}/>}
            centerElement={title}

        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView
                style={styles.container}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh.bind(this)}
                  />
                }
                >

                  <View style={{marginTop:0, backgroundColor:'#fff'}}>
                    <Subheader text="Riwayat"/>
                   {this.RenderOffline()}
                  </View>
                </ScrollView>
            </View>
        );
    }
}

DetailRiwayat.propTypes = propTypes;

export default DetailRiwayat;
