import { Text, Dimensions, NetInfo, View, StyleSheet, RefreshControl, ActivityIndicator,ScrollView, ToastAndroid, Platform, Image } from 'react-native';
import React, { Component, PropTypes } from 'react';
import routes from '../routes';
import {DevApi, MainApi} from '../Api';
import NetPage from '../Net';
import DetailMedis from '../Medis/detail';
import DetailRiwayat from '../Medis/detailriwayat';
import {IconToggle, Card, Avatar, ListItem, Subheader, Toolbar, COLOR, ThemeProvider} from '../react-native-material-ui/src';
import ImageSlider from 'react-native-image-slider';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';

import Moment from 'moment';
Moment.locale('id') //For Turkey
const welcomeImg = require('./../img/welcome.jpg');
const welcomeImg2 = require('./../img/welcome2.jpg');
const welcomeImg3 = require('./../img/welcome3.jpg');
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box:{
      width: deviceWidth / 3.1,
      height: deviceHeight / 4.5,
      alignSelf: "center",
      alignItems:'center',
      justifyContent: "center",
      backgroundColor:'#fff'
    },
    centering: {
      alignItems: 'center',
      justifyContent: 'center',
      padding: 8,
    },
    Rm:{
      marginBottom:3,
    },
    banner:{
        height: deviceHeight / 4.5,
        backgroundColor:COLOR.grey200,

    },
    actionButtonIcon: {
      fontSize: 30,
      height: 32,
    color: 'white',
  },
});

const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
};
class Medis extends Component {
  constructor(props) {
      super(props);
      this.getMedis = this.getMedis.bind(this)
      this.getMedisDetail = this.getMedisDetail.bind(this)
      this.getRiwayat = this.getRiwayat.bind(this)
      this.state = {
          selected: [],
          searchText: '',
          data: [],
          datax: [],
          loading: true,
          animating: true,
          refreshing: false,
          isConnected: true,

      };
  }
  componentDidMount() {

      var that = this;
      this.getMedis();
      this.getRiwayat();
      that.setToggleTimeout();
      NetInfo.isConnected.addEventListener(
     'change',
     this._handleConnectivityChange
     );
     NetInfo.isConnected.fetch().done(
         (isConnected) => { this.setState({isConnected}); }
     );
  }

  componentWillUnmount() {
      clearTimeout(this._timer);
    NetInfo.isConnected.removeEventListener(
      'change',
      this._handleConnectivityChange
  );
}
_handleConnectivityChange = (isConnected) => {
this.setState({
  isConnected,
});
};
  _onRefresh() {
    this.setState({refreshing: true});
    this.getMedis().then(() => {
      this.setState({refreshing: false});
    });
  }
  _onRefreshx() {
    this.setState({refreshing: true});
    this.getRiwayat().then(() => {
      this.setState({refreshing: false});
    });
  }

  setToggleTimeout() {
    this._timer = setTimeout(() => {
      this.setState({animating: !this.state.animating});
      this.setToggleTimeout();
    }, 2000);
  }
  getMedisDetail(ID, title) {
      //dismissKeyboard();
      //Alert.alert(
      //  nama_Anak
      //)
    this.props.navigator.push({
      id: ID,
      title: title,
      Page: DetailMedis,
    });
    }
    getRiwayatDetail(ID, title) {
        //dismissKeyboard();
        //Alert.alert(
        //  nama_Anak
        //)
      this.props.navigator.push({
        id: ID,
        title: title,
        Page: DetailRiwayat,
      });
      }
  getMedis() {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });

      var url =  DevApi + 'medis';
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          //componentWillUnmount();
          that.setState({ data : result,
                          loading: false

                        });
           //data : this.state.data(result.data),

        })
        .catch((error) => { console.error(error); });
  }
  getRiwayat() {
      // Set loading to true to display a Spinner
      this.setState({
          loading: true
      });

      var url =  DevApi + 'riwayat';
      var that = this;
      return fetch(url, {
              method: 'GET',
              headers: {
                'Client-Service': 'mobile-client',
                'Content-Type': 'application/json',
                'Auth-key': 'medcrestapi'
              }
        })
        .then(function(response) {
          return response.json();
        }).then(function(result) {
          //componentWillUnmount();
          that.setState({ datax : result,
                          loading: false

                        });
           //data : this.state.data(result.data),

        })
        .catch((error) => { console.error(error); });
  }
  delMedis(ID) {
    var url =  DevApi + 'medis/delete/' + ID;
    var that = this;
    return fetch(url, {
            method: 'DELETE',
            headers: {
              'Client-Service': 'mobile-client',
              'Content-Type': 'application/json',
              'Auth-key': 'medcrestapi'
            }
      })
      .then(function(response) {
        return response.json();
      }).then(function(result) {

        ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
        this.setState({
          data : result,
          loading: true
        });
        //componentWillUpdate();
         //data : this.state.data(result.data),

      })
        ToastAndroid.show(result.data.message, ToastAndroid.SHORT)
      .catch((error) => { console.error(error); });
    }

  RenderOffline(){

    if(this.state.isConnected == false){

        return(
            <View>
             <NetPage />
            </View>

        );
    }else{

       return(

         <View>
         <Subheader text="Riwayat Medis"/>

       {this.renderRiwayat()}

       <Subheader text="Medical Labs"/>
       {this.renderMedis()}
         </View>
       )
    }
  }

  renderMedis(){

    if (this.state.loading) {
      return (
        <View>
        <ActivityIndicator
        animating={this.state.animating}
        style={[styles.centering, {height: 50}]}
        size="small"
        color={COLOR.pink500}
       />
        </View>

        );

    }else{

        if(this.state.data.status == '200'){

          return(
             <View>
             {this.state.data.data.map((item, i)=>{

                return (

                 <View>
                    {this.renderItem(item.title, COLOR.tealA700, 'local-pharmacy', item.id)}
                 </View>

                );

              })}
             </View>
          );
        }else{

          return (

            <View>
              <Text style={{ alignSelf: 'center' }}>No Medis Record</Text>
            </View>


          );
        }

    }


  }
  renderRiwayat(){


    if (this.state.loading) {
      return (
        <View>
        <ActivityIndicator
        animating={this.state.animating}
        style={[styles.centering, {height: 50}]}
        size="small"
        color={COLOR.pink500}
       />
        </View>

        );

    }else{

        if(this.state.datax.status == '200'){


          return(
             <View>
             {this.state.datax.data.map((item, i)=>{

               const dateTime= item.tanggal;

               const formattedDT = Moment(dateTime).format('LL') //2 May

                return (

                 <View>

                    {this.renderItemx(item.tanggal, COLOR.tealA700, 'local-hospital', item.idriwayat)}
                 </View>

                );

              })}
             </View>
          );
        }else{

          return (

            <View>
              <Text style={{ alignSelf: 'center' }}>No Riwayat Medclabs Record</Text>
            </View>


          );
        }

    }


  }
  renderToolbar = () => {
      if (this.state.selected.length > 0) {
          return (
              <Toolbar
                  key="toolbar"
                  leftElement="info"
                  onLeftElementPress={() => this.setState({ selected: [] })}
                  centerElement='Info'
                  style={{
                      container: { backgroundColor: 'white' },
                      titleText: { color: 'rgba(0,0,0,.87)' },
                      leftElement: { color: 'rgba(0,0,0,.54)' },
                      rightElement: { color: 'rgba(0,0,0,.54)' },
                  }}
              />
          );
      }
      return (
          <Toolbar
              key="toolbar"
              leftElement="arrow-back"
              onLeftElementPress={() => this.props.navigator.pop()}
              centerElement={this.props.route.title}
              searchable={{
                  autoFocus: true,
                  placeholder: 'Search',
                  onChangeText: value => this.setState({ searchText: value }),
                  onSearchClosed: () => this.setState({ searchText: '' }),
              }}
          />
      );
  }
  renderItem = (title, color, icon, ID) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={40} color={color}/>}
            centerElement={title}
            rightElement="chevron-right"
            onPress={() => this.getMedisDetail(ID, title)}
        />

      );
  }
  renderItemx = (title, color, icon, ID) => {
      const searchText = this.state.searchText.toLowerCase();

      if (searchText.length > 0 && title.toLowerCase().indexOf(searchText) < 0) {
          return null;
      }

      return (

        <ListItem
            divider
            leftElement={<Avatar icon={icon} size={40} color={color}/>}
            centerElement={title}
            rightElement="chevron-right"
            onPress={() => this.getRiwayatDetail(ID, title)}
        />

      );
  }
    render() {
        return (
            <View style={styles.container}>
                {this.renderToolbar()}
                <ScrollView
                style={styles.container}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh.bind(this)}
                  />
                }
                >
                <ImageSlider images={[
                     welcomeImg
                 ]}/>

                    <View style={{marginTop:0, backgroundColor:'#f9f9f9'}}>


                    {this.RenderOffline()}


                    </View>

                </ScrollView>

                <ActionButton buttonColor={COLOR.pink500} bgColor="rgba(0,0,0,0.8)">
                    <ActionButton.Item buttonColor={COLOR.medcColor}  style= {styles.icon} title="Input Medis"  onPress={() => this.props.navigator.push(routes.inputmedis)}>
                      <Icon name="local-hospital" style={styles.actionButtonIcon} />
                    </ActionButton.Item>

                </ActionButton>

            </View>
        );
    }
}

Medis.propTypes = propTypes;

export default Medis;
